import 'package:freezed_annotation/freezed_annotation.dart';
part 'ware_model.g.dart';
part 'ware_model.freezed.dart';

@freezed
class Ware with _$Ware {
  factory Ware(
      {required String modelName,
      required String barcode,
      required int price,
      required int quantity,
      required String brand,
      required String color}) = _Ware;

  factory Ware.fromJson(Map<String, dynamic> json) => _$WareFromJson(json);
}
