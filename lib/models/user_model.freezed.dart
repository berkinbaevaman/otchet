// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'user_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

User _$UserFromJson(Map<String, dynamic> json) {
  switch (json['runtimeType'] as String) {
    case 'default':
      return _User.fromJson(json);
    case 'empty':
      return _UserEmpty.fromJson(json);

    default:
      throw FallThroughError();
  }
}

/// @nodoc
class _$UserTearOff {
  const _$UserTearOff();

  _User call(
      {required int id,
      required int role_id,
      required String resident,
      required int iin,
      required String name,
      required String surname,
      required String last_name,
      required String email,
      required String api_token,
      required String resident}) {
    return _User(
      id: id,
      role_id: role_id,
      resident: resident,
      iin: iin,
      name: name,
      surname: surname,
      last_name: last_name,
      email: email,
      api_token: api_token,
      resident: resident,
    );
  }

  _UserEmpty empty() {
    return const _UserEmpty();
  }

  User fromJson(Map<String, Object> json) {
    return User.fromJson(json);
  }
}

/// @nodoc
const $User = _$UserTearOff();

/// @nodoc
mixin _$User {
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function(
            int id,
            int role_id,
            String resident,
            int iin,
            String name,
            String surname,
            String last_name,
            String email,
            String api_token,
            String resident)
        $default, {
    required TResult Function() empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function(
            int id,
            int role_id,
            String resident,
            int iin,
            String name,
            String surname,
            String last_name,
            String email,
            String api_token,
            String resident)?
        $default, {
    TResult Function()? empty,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(_User value) $default, {
    required TResult Function(_UserEmpty value) empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(_User value)? $default, {
    TResult Function(_UserEmpty value)? empty,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserCopyWith<$Res> {
  factory $UserCopyWith(User value, $Res Function(User) then) =
      _$UserCopyWithImpl<$Res>;
}

/// @nodoc
class _$UserCopyWithImpl<$Res> implements $UserCopyWith<$Res> {
  _$UserCopyWithImpl(this._value, this._then);

  final User _value;
  // ignore: unused_field
  final $Res Function(User) _then;
}

/// @nodoc
abstract class _$UserCopyWith<$Res> {
  factory _$UserCopyWith(_User value, $Res Function(_User) then) =
      __$UserCopyWithImpl<$Res>;
  $Res call(
      {int id,
      int role_id,
      String resident,
      int iin,
      String name,
      String surname,
      String last_name,
      String email,
      String api_token,
      String resident});
}

/// @nodoc
class __$UserCopyWithImpl<$Res> extends _$UserCopyWithImpl<$Res>
    implements _$UserCopyWith<$Res> {
  __$UserCopyWithImpl(_User _value, $Res Function(_User) _then)
      : super(_value, (v) => _then(v as _User));

  @override
  _User get _value => super._value as _User;

  @override
  $Res call({
    Object? id = freezed,
    Object? role_id = freezed,
    Object? resident = freezed,
    Object? iin = freezed,
    Object? name = freezed,
    Object? surname = freezed,
    Object? last_name = freezed,
    Object? email = freezed,
    Object? api_token = freezed,
    Object? resident = freezed,
  }) {
    return _then(_User(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      role_id: role_id == freezed
          ? _value.role_id
          : role_id // ignore: cast_nullable_to_non_nullable
              as int,
      resident: resident == freezed
          ? _value.resident
          : resident // ignore: cast_nullable_to_non_nullable
              as String,
      iin: iin == freezed
          ? _value.iin
          : iin // ignore: cast_nullable_to_non_nullable
              as int,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      surname: surname == freezed
          ? _value.surname
          : surname // ignore: cast_nullable_to_non_nullable
              as String,
      last_name: last_name == freezed
          ? _value.last_name
          : last_name // ignore: cast_nullable_to_non_nullable
              as String,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      api_token: api_token == freezed
          ? _value.api_token
          : api_token // ignore: cast_nullable_to_non_nullable
              as String,
      resident: resident == freezed
          ? _value.resident
          : resident // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_User implements _User {
  const _$_User(
      {required this.id,
      required this.role_id,
      required this.resident,
      required this.iin,
      required this.name,
      required this.surname,
      required this.last_name,
      required this.email,
      required this.api_token,
      required this.resident});

  factory _$_User.fromJson(Map<String, dynamic> json) =>
      _$_$_UserFromJson(json);

  @override
  final int id;
  @override
  final int role_id;
  @override
  final String resident;
  @override
  final int iin;
  @override
  final String name;
  @override
  final String surname;
  @override
  final String last_name;
  @override
  final String email;
  @override
  final String api_token;
  @override
  final String resident;

  @override
  String toString() {
    return 'User(id: $id, role_id: $role_id, resident: $resident, iin: $iin, name: $name, surname: $surname, last_name: $last_name, email: $email, api_token: $api_token, resident: $resident)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _User &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.role_id, role_id) ||
                const DeepCollectionEquality()
                    .equals(other.role_id, role_id)) &&
            (identical(other.resident, resident) ||
                const DeepCollectionEquality()
                    .equals(other.resident, resident)) &&
            (identical(other.iin, iin) ||
                const DeepCollectionEquality().equals(other.iin, iin)) &&
            (identical(other.name, name) ||
                const DeepCollectionEquality().equals(other.name, name)) &&
            (identical(other.surname, surname) ||
                const DeepCollectionEquality()
                    .equals(other.surname, surname)) &&
            (identical(other.last_name, last_name) ||
                const DeepCollectionEquality()
                    .equals(other.last_name, last_name)) &&
            (identical(other.email, email) ||
                const DeepCollectionEquality().equals(other.email, email)) &&
            (identical(other.api_token, api_token) ||
                const DeepCollectionEquality()
                    .equals(other.api_token, api_token)) &&
            (identical(other.resident, resident) ||
                const DeepCollectionEquality()
                    .equals(other.resident, resident)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(role_id) ^
      const DeepCollectionEquality().hash(resident) ^
      const DeepCollectionEquality().hash(iin) ^
      const DeepCollectionEquality().hash(name) ^
      const DeepCollectionEquality().hash(surname) ^
      const DeepCollectionEquality().hash(last_name) ^
      const DeepCollectionEquality().hash(email) ^
      const DeepCollectionEquality().hash(api_token) ^
      const DeepCollectionEquality().hash(resident);

  @JsonKey(ignore: true)
  @override
  _$UserCopyWith<_User> get copyWith =>
      __$UserCopyWithImpl<_User>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function(
            int id,
            int role_id,
            String resident,
            int iin,
            String name,
            String surname,
            String last_name,
            String email,
            String api_token,
            String resident)
        $default, {
    required TResult Function() empty,
  }) {
    return $default(id, role_id, resident, iin, name, surname, last_name, email,
        api_token, resident);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function(
            int id,
            int role_id,
            String resident,
            int iin,
            String name,
            String surname,
            String last_name,
            String email,
            String api_token,
            String resident)?
        $default, {
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if ($default != null) {
      return $default(id, role_id, resident, iin, name, surname, last_name,
          email, api_token, resident);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(_User value) $default, {
    required TResult Function(_UserEmpty value) empty,
  }) {
    return $default(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(_User value)? $default, {
    TResult Function(_UserEmpty value)? empty,
    required TResult orElse(),
  }) {
    if ($default != null) {
      return $default(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$_$_UserToJson(this)..['runtimeType'] = 'default';
  }
}

abstract class _User implements User {
  const factory _User(
      {required int id,
      required int role_id,
      required String resident,
      required int iin,
      required String name,
      required String surname,
      required String last_name,
      required String email,
      required String api_token,
      required String resident}) = _$_User;

  factory _User.fromJson(Map<String, dynamic> json) = _$_User.fromJson;

  int get id => throw _privateConstructorUsedError;
  int get role_id => throw _privateConstructorUsedError;
  String get resident => throw _privateConstructorUsedError;
  int get iin => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get surname => throw _privateConstructorUsedError;
  String get last_name => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  String get api_token => throw _privateConstructorUsedError;
  String get resident => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$UserCopyWith<_User> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$UserEmptyCopyWith<$Res> {
  factory _$UserEmptyCopyWith(
          _UserEmpty value, $Res Function(_UserEmpty) then) =
      __$UserEmptyCopyWithImpl<$Res>;
}

/// @nodoc
class __$UserEmptyCopyWithImpl<$Res> extends _$UserCopyWithImpl<$Res>
    implements _$UserEmptyCopyWith<$Res> {
  __$UserEmptyCopyWithImpl(_UserEmpty _value, $Res Function(_UserEmpty) _then)
      : super(_value, (v) => _then(v as _UserEmpty));

  @override
  _UserEmpty get _value => super._value as _UserEmpty;
}

@JsonSerializable()

/// @nodoc
class _$_UserEmpty implements _UserEmpty {
  const _$_UserEmpty();

  factory _$_UserEmpty.fromJson(Map<String, dynamic> json) =>
      _$_$_UserEmptyFromJson(json);

  @override
  String toString() {
    return 'User.empty()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _UserEmpty);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function(
            int id,
            int role_id,
            String resident,
            int iin,
            String name,
            String surname,
            String last_name,
            String email,
            String api_token,
            String resident)
        $default, {
    required TResult Function() empty,
  }) {
    return empty();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function(
            int id,
            int role_id,
            String resident,
            int iin,
            String name,
            String surname,
            String last_name,
            String email,
            String api_token,
            String resident)?
        $default, {
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (empty != null) {
      return empty();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(_User value) $default, {
    required TResult Function(_UserEmpty value) empty,
  }) {
    return empty(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(_User value)? $default, {
    TResult Function(_UserEmpty value)? empty,
    required TResult orElse(),
  }) {
    if (empty != null) {
      return empty(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$_$_UserEmptyToJson(this)..['runtimeType'] = 'empty';
  }
}

abstract class _UserEmpty implements User {
  const factory _UserEmpty() = _$_UserEmpty;

  factory _UserEmpty.fromJson(Map<String, dynamic> json) =
      _$_UserEmpty.fromJson;
}

Organization _$OrganizationFromJson(Map<String, dynamic> json) {
  switch (json['runtimeType'] as String) {
    case 'default':
      return _Organization.fromJson(json);
    case 'empty':
      return _OrganizationEmpty.fromJson(json);

    default:
      throw FallThroughError();
  }
}

/// @nodoc
class _$OrganizationTearOff {
  const _$OrganizationTearOff();

  _Organization call(
      {required int id,
      required int user_id,
      required String title,
      required int bin,
      required String email,
      required String status}) {
    return _Organization(
      id: id,
      user_id: user_id,
      title: title,
      bin: bin,
      email: email,
      status: status,
    );
  }

  _OrganizationEmpty empty() {
    return const _OrganizationEmpty();
  }

  Organization fromJson(Map<String, Object> json) {
    return Organization.fromJson(json);
  }
}

/// @nodoc
const $Organization = _$OrganizationTearOff();

/// @nodoc
mixin _$Organization {
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function(int id, int user_id, String title, int bin, String email,
            String status)
        $default, {
    required TResult Function() empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function(int id, int user_id, String title, int bin, String email,
            String status)?
        $default, {
    TResult Function()? empty,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(_Organization value) $default, {
    required TResult Function(_OrganizationEmpty value) empty,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(_Organization value)? $default, {
    TResult Function(_OrganizationEmpty value)? empty,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OrganizationCopyWith<$Res> {
  factory $OrganizationCopyWith(
          Organization value, $Res Function(Organization) then) =
      _$OrganizationCopyWithImpl<$Res>;
}

/// @nodoc
class _$OrganizationCopyWithImpl<$Res> implements $OrganizationCopyWith<$Res> {
  _$OrganizationCopyWithImpl(this._value, this._then);

  final Organization _value;
  // ignore: unused_field
  final $Res Function(Organization) _then;
}

/// @nodoc
abstract class _$OrganizationCopyWith<$Res> {
  factory _$OrganizationCopyWith(
          _Organization value, $Res Function(_Organization) then) =
      __$OrganizationCopyWithImpl<$Res>;
  $Res call(
      {int id,
      int user_id,
      String title,
      int bin,
      String email,
      String status});
}

/// @nodoc
class __$OrganizationCopyWithImpl<$Res> extends _$OrganizationCopyWithImpl<$Res>
    implements _$OrganizationCopyWith<$Res> {
  __$OrganizationCopyWithImpl(
      _Organization _value, $Res Function(_Organization) _then)
      : super(_value, (v) => _then(v as _Organization));

  @override
  _Organization get _value => super._value as _Organization;

  @override
  $Res call({
    Object? id = freezed,
    Object? user_id = freezed,
    Object? title = freezed,
    Object? bin = freezed,
    Object? email = freezed,
    Object? status = freezed,
  }) {
    return _then(_Organization(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      user_id: user_id == freezed
          ? _value.user_id
          : user_id // ignore: cast_nullable_to_non_nullable
              as int,
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      bin: bin == freezed
          ? _value.bin
          : bin // ignore: cast_nullable_to_non_nullable
              as int,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String,
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_Organization implements _Organization {
  const _$_Organization(
      {required this.id,
      required this.user_id,
      required this.title,
      required this.bin,
      required this.email,
      required this.status});

  factory _$_Organization.fromJson(Map<String, dynamic> json) =>
      _$_$_OrganizationFromJson(json);

  @override
  final int id;
  @override
  final int user_id;
  @override
  final String title;
  @override
  final int bin;
  @override
  final String email;
  @override
  final String status;

  @override
  String toString() {
    return 'Organization(id: $id, user_id: $user_id, title: $title, bin: $bin, email: $email, status: $status)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Organization &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.user_id, user_id) ||
                const DeepCollectionEquality()
                    .equals(other.user_id, user_id)) &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.bin, bin) ||
                const DeepCollectionEquality().equals(other.bin, bin)) &&
            (identical(other.email, email) ||
                const DeepCollectionEquality().equals(other.email, email)) &&
            (identical(other.status, status) ||
                const DeepCollectionEquality().equals(other.status, status)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(user_id) ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(bin) ^
      const DeepCollectionEquality().hash(email) ^
      const DeepCollectionEquality().hash(status);

  @JsonKey(ignore: true)
  @override
  _$OrganizationCopyWith<_Organization> get copyWith =>
      __$OrganizationCopyWithImpl<_Organization>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function(int id, int user_id, String title, int bin, String email,
            String status)
        $default, {
    required TResult Function() empty,
  }) {
    return $default(id, user_id, title, bin, email, status);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function(int id, int user_id, String title, int bin, String email,
            String status)?
        $default, {
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if ($default != null) {
      return $default(id, user_id, title, bin, email, status);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(_Organization value) $default, {
    required TResult Function(_OrganizationEmpty value) empty,
  }) {
    return $default(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(_Organization value)? $default, {
    TResult Function(_OrganizationEmpty value)? empty,
    required TResult orElse(),
  }) {
    if ($default != null) {
      return $default(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$_$_OrganizationToJson(this)..['runtimeType'] = 'default';
  }
}

abstract class _Organization implements Organization {
  const factory _Organization(
      {required int id,
      required int user_id,
      required String title,
      required int bin,
      required String email,
      required String status}) = _$_Organization;

  factory _Organization.fromJson(Map<String, dynamic> json) =
      _$_Organization.fromJson;

  int get id => throw _privateConstructorUsedError;
  int get user_id => throw _privateConstructorUsedError;
  String get title => throw _privateConstructorUsedError;
  int get bin => throw _privateConstructorUsedError;
  String get email => throw _privateConstructorUsedError;
  String get status => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$OrganizationCopyWith<_Organization> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$OrganizationEmptyCopyWith<$Res> {
  factory _$OrganizationEmptyCopyWith(
          _OrganizationEmpty value, $Res Function(_OrganizationEmpty) then) =
      __$OrganizationEmptyCopyWithImpl<$Res>;
}

/// @nodoc
class __$OrganizationEmptyCopyWithImpl<$Res>
    extends _$OrganizationCopyWithImpl<$Res>
    implements _$OrganizationEmptyCopyWith<$Res> {
  __$OrganizationEmptyCopyWithImpl(
      _OrganizationEmpty _value, $Res Function(_OrganizationEmpty) _then)
      : super(_value, (v) => _then(v as _OrganizationEmpty));

  @override
  _OrganizationEmpty get _value => super._value as _OrganizationEmpty;
}

@JsonSerializable()

/// @nodoc
class _$_OrganizationEmpty implements _OrganizationEmpty {
  const _$_OrganizationEmpty();

  factory _$_OrganizationEmpty.fromJson(Map<String, dynamic> json) =>
      _$_$_OrganizationEmptyFromJson(json);

  @override
  String toString() {
    return 'Organization.empty()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _OrganizationEmpty);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>(
    TResult Function(int id, int user_id, String title, int bin, String email,
            String status)
        $default, {
    required TResult Function() empty,
  }) {
    return empty();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>(
    TResult Function(int id, int user_id, String title, int bin, String email,
            String status)?
        $default, {
    TResult Function()? empty,
    required TResult orElse(),
  }) {
    if (empty != null) {
      return empty();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>(
    TResult Function(_Organization value) $default, {
    required TResult Function(_OrganizationEmpty value) empty,
  }) {
    return empty(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>(
    TResult Function(_Organization value)? $default, {
    TResult Function(_OrganizationEmpty value)? empty,
    required TResult orElse(),
  }) {
    if (empty != null) {
      return empty(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$_$_OrganizationEmptyToJson(this)..['runtimeType'] = 'empty';
  }
}

abstract class _OrganizationEmpty implements Organization {
  const factory _OrganizationEmpty() = _$_OrganizationEmpty;

  factory _OrganizationEmpty.fromJson(Map<String, dynamic> json) =
      _$_OrganizationEmpty.fromJson;
}
