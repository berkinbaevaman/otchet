// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_User _$_$_UserFromJson(Map<String, dynamic> json) {
  return _$_User(
    id: json['id'] as int,
    role_id: json['role_id'] as int,
    resident: json['resident'] as String,
    iin: json['iin'] as int,
    name: json['name'] as String,
    surname: json['surname'] as String,
    last_name: json['last_name'] as String,
    email: json['email'] as String,
    api_token: json['api_token'] as String,
    resident: json['resident'] as String,
  );
}

Map<String, dynamic> _$_$_UserToJson(_$_User instance) => <String, dynamic>{
      'id': instance.id,
      'role_id': instance.role_id,
      'iin': instance.iin,
      'name': instance.name,
      'surname': instance.surname,
      'last_name': instance.last_name,
      'email': instance.email,
      'api_token': instance.api_token,
      'resident': instance.resident,
    };

_$_UserEmpty _$_$_UserEmptyFromJson(Map<String, dynamic> json) {
  return _$_UserEmpty();
}

Map<String, dynamic> _$_$_UserEmptyToJson(_$_UserEmpty instance) =>
    <String, dynamic>{};

_$_Organization _$_$_OrganizationFromJson(Map<String, dynamic> json) {
  return _$_Organization(
    id: json['id'] as int,
    user_id: json['user_id'] as int,
    title: json['title'] as String,
    bin: json['bin'] as int,
    email: json['email'] as String,
    status: json['status'] as String,
  );
}

Map<String, dynamic> _$_$_OrganizationToJson(_$_Organization instance) =>
    <String, dynamic>{
      'id': instance.id,
      'user_id': instance.user_id,
      'title': instance.title,
      'bin': instance.bin,
      'email': instance.email,
      'status': instance.status,
    };

_$_OrganizationEmpty _$_$_OrganizationEmptyFromJson(Map<String, dynamic> json) {
  return _$_OrganizationEmpty();
}

Map<String, dynamic> _$_$_OrganizationEmptyToJson(
        _$_OrganizationEmpty instance) =>
    <String, dynamic>{};
