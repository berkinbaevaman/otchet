import 'package:equatable/equatable.dart';

class Accesses extends Equatable {
  final bool taxes;
  final bool warehouse;
  final bool cashier;
  final bool docs;

  const Accesses({
    required this.taxes,
    required this.warehouse,
    required this.cashier,
    required this.docs,
  });

  Accesses copyWith({
    bool? taxes,
    bool? warehouse,
    bool? cashier,
    bool? docs,
  }) {
    return Accesses(
        cashier: cashier ?? this.cashier,
        warehouse: warehouse ?? this.warehouse,
        taxes: taxes ?? this.taxes,
        docs: docs ?? this.docs);
  }

  @override
  List<Object?> get props => [taxes, warehouse, cashier, docs];
}
