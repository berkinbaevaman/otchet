// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ware_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Ware _$_$_WareFromJson(Map<String, dynamic> json) {
  return _$_Ware(
    modelName: json['modelName'] as String,
    barcode: json['barcode'] as String,
    price: json['price'] as int,
    quantity: json['quantity'] as int,
    brand: json['brand'] as String,
    color: json['color'] as String,
  );
}

Map<String, dynamic> _$_$_WareToJson(_$_Ware instance) => <String, dynamic>{
      'modelName': instance.modelName,
      'barcode': instance.barcode,
      'price': instance.price,
      'quantity': instance.quantity,
      'brand': instance.brand,
      'color': instance.color,
    };
