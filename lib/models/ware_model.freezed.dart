// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'ware_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Ware _$WareFromJson(Map<String, dynamic> json) {
  return _Ware.fromJson(json);
}

/// @nodoc
class _$WareTearOff {
  const _$WareTearOff();

  _Ware call(
      {required String modelName,
      required String barcode,
      required int price,
      required int quantity,
      required String brand,
      required String color}) {
    return _Ware(
      modelName: modelName,
      barcode: barcode,
      price: price,
      quantity: quantity,
      brand: brand,
      color: color,
    );
  }

  Ware fromJson(Map<String, Object> json) {
    return Ware.fromJson(json);
  }
}

/// @nodoc
const $Ware = _$WareTearOff();

/// @nodoc
mixin _$Ware {
  String get modelName => throw _privateConstructorUsedError;
  String get barcode => throw _privateConstructorUsedError;
  int get price => throw _privateConstructorUsedError;
  int get quantity => throw _privateConstructorUsedError;
  String get brand => throw _privateConstructorUsedError;
  String get color => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $WareCopyWith<Ware> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WareCopyWith<$Res> {
  factory $WareCopyWith(Ware value, $Res Function(Ware) then) =
      _$WareCopyWithImpl<$Res>;
  $Res call(
      {String modelName,
      String barcode,
      int price,
      int quantity,
      String brand,
      String color});
}

/// @nodoc
class _$WareCopyWithImpl<$Res> implements $WareCopyWith<$Res> {
  _$WareCopyWithImpl(this._value, this._then);

  final Ware _value;
  // ignore: unused_field
  final $Res Function(Ware) _then;

  @override
  $Res call({
    Object? modelName = freezed,
    Object? barcode = freezed,
    Object? price = freezed,
    Object? quantity = freezed,
    Object? brand = freezed,
    Object? color = freezed,
  }) {
    return _then(_value.copyWith(
      modelName: modelName == freezed
          ? _value.modelName
          : modelName // ignore: cast_nullable_to_non_nullable
              as String,
      barcode: barcode == freezed
          ? _value.barcode
          : barcode // ignore: cast_nullable_to_non_nullable
              as String,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as int,
      quantity: quantity == freezed
          ? _value.quantity
          : quantity // ignore: cast_nullable_to_non_nullable
              as int,
      brand: brand == freezed
          ? _value.brand
          : brand // ignore: cast_nullable_to_non_nullable
              as String,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$WareCopyWith<$Res> implements $WareCopyWith<$Res> {
  factory _$WareCopyWith(_Ware value, $Res Function(_Ware) then) =
      __$WareCopyWithImpl<$Res>;
  @override
  $Res call(
      {String modelName,
      String barcode,
      int price,
      int quantity,
      String brand,
      String color});
}

/// @nodoc
class __$WareCopyWithImpl<$Res> extends _$WareCopyWithImpl<$Res>
    implements _$WareCopyWith<$Res> {
  __$WareCopyWithImpl(_Ware _value, $Res Function(_Ware) _then)
      : super(_value, (v) => _then(v as _Ware));

  @override
  _Ware get _value => super._value as _Ware;

  @override
  $Res call({
    Object? modelName = freezed,
    Object? barcode = freezed,
    Object? price = freezed,
    Object? quantity = freezed,
    Object? brand = freezed,
    Object? color = freezed,
  }) {
    return _then(_Ware(
      modelName: modelName == freezed
          ? _value.modelName
          : modelName // ignore: cast_nullable_to_non_nullable
              as String,
      barcode: barcode == freezed
          ? _value.barcode
          : barcode // ignore: cast_nullable_to_non_nullable
              as String,
      price: price == freezed
          ? _value.price
          : price // ignore: cast_nullable_to_non_nullable
              as int,
      quantity: quantity == freezed
          ? _value.quantity
          : quantity // ignore: cast_nullable_to_non_nullable
              as int,
      brand: brand == freezed
          ? _value.brand
          : brand // ignore: cast_nullable_to_non_nullable
              as String,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_Ware implements _Ware {
  _$_Ware(
      {required this.modelName,
      required this.barcode,
      required this.price,
      required this.quantity,
      required this.brand,
      required this.color});

  factory _$_Ware.fromJson(Map<String, dynamic> json) =>
      _$_$_WareFromJson(json);

  @override
  final String modelName;
  @override
  final String barcode;
  @override
  final int price;
  @override
  final int quantity;
  @override
  final String brand;
  @override
  final String color;

  @override
  String toString() {
    return 'Ware(modelName: $modelName, barcode: $barcode, price: $price, quantity: $quantity, brand: $brand, color: $color)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Ware &&
            (identical(other.modelName, modelName) ||
                const DeepCollectionEquality()
                    .equals(other.modelName, modelName)) &&
            (identical(other.barcode, barcode) ||
                const DeepCollectionEquality()
                    .equals(other.barcode, barcode)) &&
            (identical(other.price, price) ||
                const DeepCollectionEquality().equals(other.price, price)) &&
            (identical(other.quantity, quantity) ||
                const DeepCollectionEquality()
                    .equals(other.quantity, quantity)) &&
            (identical(other.brand, brand) ||
                const DeepCollectionEquality().equals(other.brand, brand)) &&
            (identical(other.color, color) ||
                const DeepCollectionEquality().equals(other.color, color)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(modelName) ^
      const DeepCollectionEquality().hash(barcode) ^
      const DeepCollectionEquality().hash(price) ^
      const DeepCollectionEquality().hash(quantity) ^
      const DeepCollectionEquality().hash(brand) ^
      const DeepCollectionEquality().hash(color);

  @JsonKey(ignore: true)
  @override
  _$WareCopyWith<_Ware> get copyWith =>
      __$WareCopyWithImpl<_Ware>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_WareToJson(this);
  }
}

abstract class _Ware implements Ware {
  factory _Ware(
      {required String modelName,
      required String barcode,
      required int price,
      required int quantity,
      required String brand,
      required String color}) = _$_Ware;

  factory _Ware.fromJson(Map<String, dynamic> json) = _$_Ware.fromJson;

  @override
  String get modelName => throw _privateConstructorUsedError;
  @override
  String get barcode => throw _privateConstructorUsedError;
  @override
  int get price => throw _privateConstructorUsedError;
  @override
  int get quantity => throw _privateConstructorUsedError;
  @override
  String get brand => throw _privateConstructorUsedError;
  @override
  String get color => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$WareCopyWith<_Ware> get copyWith => throw _privateConstructorUsedError;
}
