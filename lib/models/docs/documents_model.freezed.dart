// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides

part of 'documents_model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Documents _$DocumentsFromJson(Map<String, dynamic> json) {
  return _Documents.fromJson(json);
}

/// @nodoc
class _$DocumentsTearOff {
  const _$DocumentsTearOff();

  _Documents call(
      {required String customerOrganizationName,
      required int bin,
      required String address,
      required String assignment,
      required int documentNumber,
      required DocType type,
      required String assignmentDate}) {
    return _Documents(
      customerOrganizationName: customerOrganizationName,
      bin: bin,
      address: address,
      assignment: assignment,
      documentNumber: documentNumber,
      type: type,
      assignmentDate: assignmentDate,
    );
  }

  Documents fromJson(Map<String, Object> json) {
    return Documents.fromJson(json);
  }
}

/// @nodoc
const $Documents = _$DocumentsTearOff();

/// @nodoc
mixin _$Documents {
  String get customerOrganizationName => throw _privateConstructorUsedError;
  int get bin => throw _privateConstructorUsedError;
  String get address => throw _privateConstructorUsedError;
  String get assignment => throw _privateConstructorUsedError;
  int get documentNumber => throw _privateConstructorUsedError;
  DocType get type => throw _privateConstructorUsedError;
  String get assignmentDate => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $DocumentsCopyWith<Documents> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $DocumentsCopyWith<$Res> {
  factory $DocumentsCopyWith(Documents value, $Res Function(Documents) then) =
      _$DocumentsCopyWithImpl<$Res>;
  $Res call(
      {String customerOrganizationName,
      int bin,
      String address,
      String assignment,
      int documentNumber,
      DocType type,
      String assignmentDate});
}

/// @nodoc
class _$DocumentsCopyWithImpl<$Res> implements $DocumentsCopyWith<$Res> {
  _$DocumentsCopyWithImpl(this._value, this._then);

  final Documents _value;
  // ignore: unused_field
  final $Res Function(Documents) _then;

  @override
  $Res call({
    Object? customerOrganizationName = freezed,
    Object? bin = freezed,
    Object? address = freezed,
    Object? assignment = freezed,
    Object? documentNumber = freezed,
    Object? type = freezed,
    Object? assignmentDate = freezed,
  }) {
    return _then(_value.copyWith(
      customerOrganizationName: customerOrganizationName == freezed
          ? _value.customerOrganizationName
          : customerOrganizationName // ignore: cast_nullable_to_non_nullable
              as String,
      bin: bin == freezed
          ? _value.bin
          : bin // ignore: cast_nullable_to_non_nullable
              as int,
      address: address == freezed
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String,
      assignment: assignment == freezed
          ? _value.assignment
          : assignment // ignore: cast_nullable_to_non_nullable
              as String,
      documentNumber: documentNumber == freezed
          ? _value.documentNumber
          : documentNumber // ignore: cast_nullable_to_non_nullable
              as int,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as DocType,
      assignmentDate: assignmentDate == freezed
          ? _value.assignmentDate
          : assignmentDate // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$DocumentsCopyWith<$Res> implements $DocumentsCopyWith<$Res> {
  factory _$DocumentsCopyWith(
          _Documents value, $Res Function(_Documents) then) =
      __$DocumentsCopyWithImpl<$Res>;
  @override
  $Res call(
      {String customerOrganizationName,
      int bin,
      String address,
      String assignment,
      int documentNumber,
      DocType type,
      String assignmentDate});
}

/// @nodoc
class __$DocumentsCopyWithImpl<$Res> extends _$DocumentsCopyWithImpl<$Res>
    implements _$DocumentsCopyWith<$Res> {
  __$DocumentsCopyWithImpl(_Documents _value, $Res Function(_Documents) _then)
      : super(_value, (v) => _then(v as _Documents));

  @override
  _Documents get _value => super._value as _Documents;

  @override
  $Res call({
    Object? customerOrganizationName = freezed,
    Object? bin = freezed,
    Object? address = freezed,
    Object? assignment = freezed,
    Object? documentNumber = freezed,
    Object? type = freezed,
    Object? assignmentDate = freezed,
  }) {
    return _then(_Documents(
      customerOrganizationName: customerOrganizationName == freezed
          ? _value.customerOrganizationName
          : customerOrganizationName // ignore: cast_nullable_to_non_nullable
              as String,
      bin: bin == freezed
          ? _value.bin
          : bin // ignore: cast_nullable_to_non_nullable
              as int,
      address: address == freezed
          ? _value.address
          : address // ignore: cast_nullable_to_non_nullable
              as String,
      assignment: assignment == freezed
          ? _value.assignment
          : assignment // ignore: cast_nullable_to_non_nullable
              as String,
      documentNumber: documentNumber == freezed
          ? _value.documentNumber
          : documentNumber // ignore: cast_nullable_to_non_nullable
              as int,
      type: type == freezed
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as DocType,
      assignmentDate: assignmentDate == freezed
          ? _value.assignmentDate
          : assignmentDate // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

@JsonSerializable()

/// @nodoc
class _$_Documents implements _Documents {
  _$_Documents(
      {required this.customerOrganizationName,
      required this.bin,
      required this.address,
      required this.assignment,
      required this.documentNumber,
      required this.type,
      required this.assignmentDate});

  factory _$_Documents.fromJson(Map<String, dynamic> json) =>
      _$_$_DocumentsFromJson(json);

  @override
  final String customerOrganizationName;
  @override
  final int bin;
  @override
  final String address;
  @override
  final String assignment;
  @override
  final int documentNumber;
  @override
  final DocType type;
  @override
  final String assignmentDate;

  @override
  String toString() {
    return 'Documents(customerOrganizationName: $customerOrganizationName, bin: $bin, address: $address, assignment: $assignment, documentNumber: $documentNumber, type: $type, assignmentDate: $assignmentDate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Documents &&
            (identical(
                    other.customerOrganizationName, customerOrganizationName) ||
                const DeepCollectionEquality().equals(
                    other.customerOrganizationName,
                    customerOrganizationName)) &&
            (identical(other.bin, bin) ||
                const DeepCollectionEquality().equals(other.bin, bin)) &&
            (identical(other.address, address) ||
                const DeepCollectionEquality()
                    .equals(other.address, address)) &&
            (identical(other.assignment, assignment) ||
                const DeepCollectionEquality()
                    .equals(other.assignment, assignment)) &&
            (identical(other.documentNumber, documentNumber) ||
                const DeepCollectionEquality()
                    .equals(other.documentNumber, documentNumber)) &&
            (identical(other.type, type) ||
                const DeepCollectionEquality().equals(other.type, type)) &&
            (identical(other.assignmentDate, assignmentDate) ||
                const DeepCollectionEquality()
                    .equals(other.assignmentDate, assignmentDate)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(customerOrganizationName) ^
      const DeepCollectionEquality().hash(bin) ^
      const DeepCollectionEquality().hash(address) ^
      const DeepCollectionEquality().hash(assignment) ^
      const DeepCollectionEquality().hash(documentNumber) ^
      const DeepCollectionEquality().hash(type) ^
      const DeepCollectionEquality().hash(assignmentDate);

  @JsonKey(ignore: true)
  @override
  _$DocumentsCopyWith<_Documents> get copyWith =>
      __$DocumentsCopyWithImpl<_Documents>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_DocumentsToJson(this);
  }
}

abstract class _Documents implements Documents {
  factory _Documents(
      {required String customerOrganizationName,
      required int bin,
      required String address,
      required String assignment,
      required int documentNumber,
      required DocType type,
      required String assignmentDate}) = _$_Documents;

  factory _Documents.fromJson(Map<String, dynamic> json) =
      _$_Documents.fromJson;

  @override
  String get customerOrganizationName => throw _privateConstructorUsedError;
  @override
  int get bin => throw _privateConstructorUsedError;
  @override
  String get address => throw _privateConstructorUsedError;
  @override
  String get assignment => throw _privateConstructorUsedError;
  @override
  int get documentNumber => throw _privateConstructorUsedError;
  @override
  DocType get type => throw _privateConstructorUsedError;
  @override
  String get assignmentDate => throw _privateConstructorUsedError;
  @override
  @JsonKey(ignore: true)
  _$DocumentsCopyWith<_Documents> get copyWith =>
      throw _privateConstructorUsedError;
}
