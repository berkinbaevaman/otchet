// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'documents_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Documents _$_$_DocumentsFromJson(Map<String, dynamic> json) {
  return _$_Documents(
    customerOrganizationName: json['customerOrganizationName'] as String,
    bin: json['bin'] as int,
    address: json['address'] as String,
    assignment: json['assignment'] as String,
    documentNumber: json['documentNumber'] as int,
    type: _$enumDecode(_$DocTypeEnumMap, json['type']),
    assignmentDate: json['assignmentDate'] as String,
  );
}

Map<String, dynamic> _$_$_DocumentsToJson(_$_Documents instance) =>
    <String, dynamic>{
      'customerOrganizationName': instance.customerOrganizationName,
      'bin': instance.bin,
      'address': instance.address,
      'assignment': instance.assignment,
      'documentNumber': instance.documentNumber,
      'type': _$DocTypeEnumMap[instance.type],
      'assignmentDate': instance.assignmentDate,
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

const _$DocTypeEnumMap = {
  DocType.one: 'one',
  DocType.two: 'two',
  DocType.three: 'three',
  DocType.four: 'four',
  DocType.five: 'five',
  DocType.six: 'six',
  DocType.seven: 'seven',
};
