import 'package:freezed_annotation/freezed_annotation.dart';
part 'documents_model.g.dart';
part 'documents_model.freezed.dart';

@freezed
class Documents with _$Documents {
  factory Documents(
      {required String customerOrganizationName,
      required int bin,
      required String address,
      required String assignment,
      required int documentNumber,
      required DocType type,
      required String assignmentDate}) = _Documents;

  factory Documents.fromJson(Map<String, dynamic> json) =>
      _$DocumentsFromJson(json);
}

enum DocType {
  @JsonValue("one")
  one,
  @JsonValue("two")
  two,
  @JsonValue("three")
  three,
  @JsonValue("four")
  four,
  @JsonValue("five")
  five,
  @JsonValue("six")
  six,
  @JsonValue("seven")
  seven
}
