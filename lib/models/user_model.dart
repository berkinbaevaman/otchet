import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_model.freezed.dart';
part 'user_model.g.dart';

@freezed
class User with _$User {
  const factory User({
    required int id,
    required int role_id,
    required String resident,
    required int iin,
    required String name,
    required String surname,
    required String last_name,
    required String email,
    required String api_token,
    required String resident,
  }) = _User;
  const factory User.empty() = _UserEmpty;

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  // static const empty = User(api_token: '-', phone: '-', name: '-', balance: 0);
}

@freezed
class Organization with _$Organization {
  const factory Organization({
    required int id,
    required int user_id,
    required String title,
    required int bin,
    required String email,
    required String status,
  }) = _Organization;
  const factory Organization.empty() = _OrganizationEmpty;

  factory Organization.fromJson(Map<String, dynamic> json) =>
      _$OrganizationFromJson(json);

// static const empty = User(api_token: '-', phone: '-', name: '-', balance: 0);
}
