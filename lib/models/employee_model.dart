import 'package:equatable/equatable.dart';
import 'package:otchet/models/accesses_model.dart';

class Employee extends Equatable {
  final String name;
  final String salary;
  final String identificationNumber;
  final String password = '1245';
  final Accesses accesses = const Accesses(
      taxes: false, warehouse: false, cashier: false, docs: false);

  const Employee(this.name, this.salary, this.identificationNumber);
  static const empty = Employee('-', '-', '-');

  Employee.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        salary = json['salary'],
        identificationNumber = json['identificationNumber'];

  Employee copyWith(
      {String? name,
      String? salary,
      String? identificationNumber,
      Accesses? accesses}) {
    return Employee(name ?? this.name, salary ?? this.salary,
        identificationNumber ?? this.identificationNumber);
  }

  @override
  toString() {
    if (identificationNumber != '-') {
      return 'Имя: $name ИИН: $identificationNumber Заработная плата: $salary';
    } else {
      return 'empty Employee object}';
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['name'] = this.name;
    data['salary'] = this.salary;
    data['identificationNumber'] = this.identificationNumber;
    return data;
  }

  @override
  List<Object?> get props => [name, salary, identificationNumber];

  //toJson fromJson
}
