import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// random weight is FontWeight.w400
abstract class CustomTheme {
  static TextStyle buttonText = TextStyle(fontSize: 16, color: Colors.white);
  static BoxDecoration shadowDecoration = BoxDecoration(
    borderRadius: BorderRadius.all(Radius.circular(10)),
    boxShadow: [
      BoxShadow(
          color: Color.fromRGBO(0, 0, 0, 0.1),
          offset: Offset(0, 2),
          blurRadius: 12)
    ],
    color: Colors.white,
  );
  static BoxDecoration shadowDecorationStrong = BoxDecoration(
    borderRadius: BorderRadius.all(Radius.circular(10)),
    boxShadow: [
      BoxShadow(
          color: Color.fromRGBO(0, 0, 0, 0.2),
          offset: Offset(0, 2),
          blurRadius: 12)
    ],
    color: Colors.white,
  );

  static Color green = Color(0xFF59B439);
}

class LoadingCircle extends StatelessWidget {
  final Color color;

  const LoadingCircle({Key? key, this.color = Colors.white}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 20,
      width: 20,
      child: CircularProgressIndicator(
        valueColor: new AlwaysStoppedAnimation<Color>(color),
      ),
    );
  }
}
