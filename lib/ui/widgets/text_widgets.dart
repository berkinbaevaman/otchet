import 'package:flutter/material.dart';

class MainTitleText extends StatelessWidget {
  final String text;

  const MainTitleText(this.text) : super();
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(fontSize: 34, fontWeight: FontWeight.w700),
    );
  }
}

class SubtitleText extends StatelessWidget {
  final String text;

  const SubtitleText(this.text) : super();
  @override
  Widget build(BuildContext context) {
    return Text(text, style: TextStyle(fontSize: 12));
  }
}

class LightSubtitleText extends StatelessWidget {
  final String text;

  const LightSubtitleText(this.text) : super();
  @override
  Widget build(BuildContext context) {
    return Text(text, style: TextStyle(fontSize: 12, color: Color(0xFF919191)));
  }
}

class LightHeaderText extends StatelessWidget {
  final String text;

  const LightHeaderText(this.text) : super();
  @override
  Widget build(BuildContext context) {
    return Text(text,
        style: TextStyle(
            fontSize: 13,
            fontWeight: FontWeight.w400,
            color: Color(0xFF919191)));
  }
}

class TitleText extends StatelessWidget {
  final String text;

  const TitleText(this.text) : super();
  @override
  Widget build(BuildContext context) {
    return Text(text,
        style: TextStyle(fontSize: 17, fontWeight: FontWeight.w700));
  }
}

class HeaderText extends StatelessWidget {
  final String text;

  const HeaderText(this.text) : super();
  @override
  Widget build(BuildContext context) {
    return Text(text,
        style: TextStyle(fontSize: 28, fontWeight: FontWeight.w700));
  }
}

class RegularText extends StatelessWidget {
  final String text;
  final Color colors;
  final TextAlign? align;

  const RegularText(this.text, {this.colors = Colors.black, this.align})
      : super();
  @override
  Widget build(BuildContext context) {
    return Text(text,
        textAlign: align,
        style: TextStyle(
            fontSize: 15, fontWeight: FontWeight.w600, color: colors));
  }
}

class ColoredButtonText extends StatelessWidget {
  final String text;
  final Color color;

  const ColoredButtonText({required this.text, required this.color}) : super();
  @override
  Widget build(BuildContext context) {
    return Text(text,
        style:
            TextStyle(fontSize: 15, color: color, fontWeight: FontWeight.w500));
  }
}
