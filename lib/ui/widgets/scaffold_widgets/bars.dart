import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otchet/blocs/navigation_bloc/navigation_bloc.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

part 'appbar.dart';
part 'navbar.dart';
