part of '../scaffold_widgets/bars.dart';

@immutable
class AppBarCustom extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final Widget? action;

  const AppBarCustom({Key? key, required this.title, this.action})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      brightness: Brightness.light,
      automaticallyImplyLeading: true,
      elevation: 0.0,
      title: TitleText(title),
      centerTitle: true,
      actions: [action ?? Container()],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
