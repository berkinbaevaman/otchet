import 'package:flutter/material.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/bars.dart';

class ProtoScaffold extends StatelessWidget {
  final String appBarTitle;
  final Color color;
  final Widget body;
  final Widget? navBar;
  final Widget? action;
  const ProtoScaffold(
      {Key? key,
      required this.appBarTitle,
      required this.body,
      this.color = Colors.white,
      this.navBar,
      this.action})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      backgroundColor: color,
      appBar: AppBarCustom(
        title: appBarTitle,
        action: action,
      ),
      body: body,
      bottomNavigationBar: navBar,
    );
  }
}
