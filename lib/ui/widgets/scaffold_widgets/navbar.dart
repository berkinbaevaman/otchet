part of 'bars.dart';

class CustomNavigationBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NavigationBloc, UIState>(builder: (context, snapshot) {
      return BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        selectedIconTheme: IconThemeData(color: Colors.blue),
        showSelectedLabels: true,
        selectedLabelStyle: TextStyle(color: Colors.blue),
        // selectedLabelStyle: TextStyle(e),
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Главная',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.list_alt),
            label: 'Касса',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.insert_drive_file_outlined),
            label: 'Документы',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person_pin),
            label: 'Профиль',
          ),
        ],
        currentIndex: snapshot.index,
        selectedItemColor: Colors.blue,
        iconSize: 28,

        unselectedFontSize: 10,
        selectedFontSize: 10,
        showUnselectedLabels: true,
        unselectedItemColor: Color(0xFF000000),
        // backgroundColor: Colors.blue,
        onTap: (index) {
          BlocProvider.of<NavigationBloc>(context).add(UpdateIndex(index));

          // if (index != 0 && index != 3) {
          //   dialogSoon(context);
          // } else {
          //   BlocProvider.of<NavigationBloc>(context).add(UpdateIndex(index));
          // }

          // uiController.changeIndex(index);
        },
      );
    });
  }
}

dialogSoon(context) {
  showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
            title: new Text("В разработке"),
            content: new Text(
                "Данный раздел находится в разработке. Будет доступным в скором времени!"),
            actions: <Widget>[
              CupertinoButton.filled(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text("Понятно"),
              )
            ],
          ));
}
