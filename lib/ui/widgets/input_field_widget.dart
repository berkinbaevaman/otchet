import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomTextField extends StatelessWidget {
  final String? hintText;
  final prefixIcon;
  final onChanged;
  final onIconPressed;
  final bool obscure;
  final int? maxChar;
  final TextEditingController? controller;
  final List<TextInputFormatter>? inputFormatter;
  final String? headText;
  final TextInputType? inputType;
  final int maxLine;
  final IconData? icon;
  final FocusNode? focusNode;

  CustomTextField(
      {Key? key,
      this.hintText,
      this.prefixIcon,
      this.obscure = false,
      this.onChanged,
      this.onIconPressed,
      this.controller,
      this.headText,
      this.maxChar,
      this.inputType,
      this.maxLine = 1,
      this.inputFormatter,
      this.icon,
      this.focusNode});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (headText != null)
              Padding(
                padding: const EdgeInsets.only(left: 2.0),
                child: Text('$headText'),
              ),
            SizedBox(
              height: 8,
            ),
            Container(
              height: 45.0 + maxLine * 5,
              padding: EdgeInsets.only(top: 6),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
                border: Border.all(width: 0, color: Colors.white),
                color: Color(0xFFF7F7F7),
              ),
              child: TextField(
                controller: controller,
                cursorColor: Colors.black38,
                enableInteractiveSelection: false,
                focusNode: focusNode,
                // textAlign: TextAlign.,
                obscureText: obscure,

                textAlignVertical: TextAlignVertical.center,
                maxLength: maxChar,
                // scrollPadding: EdgeInsets.zero,
                inputFormatters: inputFormatter,
                // enabled: false,
                maxLengthEnforcement: MaxLengthEnforcement.enforced,
                maxLines: maxLine,
                decoration: new InputDecoration(
                  counter: Container(
                    height: 0,
                    width: 0,
                  ),
                  contentPadding: EdgeInsets.only(left: 16),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide.none,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide.none,
                  ),
                  disabledBorder: OutlineInputBorder(
                    borderSide: BorderSide.none,
                  ),
                  hintText: hintText,
                  suffixIcon: (icon != null)
                      ? InkWell(
                          onTap: onIconPressed,
                          child: Icon(
                            obscure ? Icons.remove_red_eye_outlined : icon,
                            color: Colors.blue,
                          ),
                        )
                      : null,
                  border: new OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    // borderSide: new BorderSide(color: Colors.transparent),
                  ),
                ),
                keyboardType: this.inputType != null
                    ? this.inputType
                    : TextInputType.emailAddress,
                style: new TextStyle(
                  color: Colors.black,
                ),
                onChanged: onChanged,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

//the class placed below should be replaced or completed
class DumbField extends StatelessWidget {
  final String dataText;
  final prefixIcon;
  final bool obscure;
  final String? headText;
  final IconData? icon;

  DumbField({
    Key? key,
    required this.dataText,
    this.prefixIcon,
    this.obscure = false,
    this.headText,
    this.icon,
  });
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (headText != null) Text(headText!),
          SizedBox(
            height: 8,
          ),
          Container(
            height: 50.0,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(
                Radius.circular(10),
              ),
              border: Border.all(width: 0, color: Colors.white),
              color: Color(0xFFF7F7F7),
            ),
            child: Padding(
              padding: const EdgeInsets.only(left: 16.0, right: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    dataText,
                    style: TextStyle(
                        color: Colors.black87.withOpacity(0.6), fontSize: 16),
                  ),
                  Icon(
                    icon,
                    color: Colors.blue,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
