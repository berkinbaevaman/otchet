import 'package:flutter/material.dart';
import 'package:otchet/ui/theme/styles.dart';

class ShadowedContainer extends StatelessWidget {
  final Widget child;
  final EdgeInsets? padding;
  final EdgeInsets? margin;
  final double? width;
  final double? height;

  const ShadowedContainer(
      {Key? key,
      required this.child,
      this.padding,
      this.margin,
      this.width,
      this.height})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      // height: height,
      padding: padding ?? const EdgeInsets.all(16),
      margin: margin ?? EdgeInsets.symmetric(horizontal: 16.0, vertical: 10),
      decoration: CustomTheme.shadowDecoration,
      child: child,
    );
  }
}
