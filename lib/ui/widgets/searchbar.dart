import 'package:flutter/material.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';

class SearchBar extends StatelessWidget {
  final bool isPortrait = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Divider(
          height: 1,
          color: Colors.grey.withOpacity(0.1),
        ),
        Container(
          height: 60,
          margin: EdgeInsets.symmetric(horizontal: 10),
          child: Row(
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width - 60,
                child: FloatingSearchBar(
                  hint: 'Search...',
                  elevation: 0.0,
                  automaticallyImplyBackButton: false,
                  automaticallyImplyDrawerHamburger: false,
                  // shadowColor: Colors.white,
                  backdropColor: Colors.white,
                  actions: [],
                  backgroundColor: Colors.grey.shade100,
                  scrollPadding: const EdgeInsets.only(top: 16, bottom: 56),
                  transitionDuration: const Duration(milliseconds: 800),
                  transitionCurve: Curves.easeInOut,
                  physics: const NeverScrollableScrollPhysics(),
                  axisAlignment: isPortrait ? 0.0 : -1.0,
                  openAxisAlignment: 0.0,
                  width: 600,
                  debounceDelay: const Duration(milliseconds: 500),
                  onQueryChanged: (query) {
                    // Call your model, bloc, controller here.
                  },
                  // Specify a custom transition to be used for
                  // animating between opened and closed stated.
                  transition: CircularFloatingSearchBarTransition(),
                  leadingActions: [
                    FloatingSearchBarAction(
                      showIfOpened: false,
                      child: CircularButton(
                        icon: const Icon(Icons.place),
                        onPressed: () {},
                      ),
                    ),
                    FloatingSearchBarAction.searchToClear(
                      showIfClosed: false,
                    ),
                  ],
                  builder: (context, transition) {
                    return ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: Material(
                        color: Colors.white,
                        elevation: 4.0,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: Colors.accents.map((color) {
                            return Container(height: 112, color: color);
                          }).toList(),
                        ),
                      ),
                    );
                  },
                ),
              ),
              Icon(
                Icons.filter_alt_outlined,
                size: 30,
              )
            ],
          ),
        ),
        SizedBox(height: 8.0),
      ],
    );
  }
}
