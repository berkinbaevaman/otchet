import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:otchet/ui/widgets/input_field_widget.dart';

class ECPField extends StatelessWidget {
  final String headText;
  const ECPField(this.headText);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        FilePickerResult? result = await FilePicker.platform.pickFiles();

        if (result != null) {
          File file = File(result.files.single.path!);
        } else {
          // User canceled the picker
        }
      },
      child: DumbField(
        dataText: 'Выбрать файл',
        headText: headText,
        icon: Icons.assignment_outlined,
      ),
    );
  }
}
