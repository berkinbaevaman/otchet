import 'package:flutter/material.dart';

class NewsTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return SizedBox(
      width: width,
      height: width * 0.375,
      child: ListView.builder(
        padding: EdgeInsets.symmetric(horizontal: 8),
        shrinkWrap: true,
        itemBuilder: (context, i) {
          if (i == 4) {
            return SizedBox(
              height: 10,
              width: 16,
            );
          }
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.asset('assets/category.png'),
          );
        },
        scrollDirection: Axis.horizontal,
        itemCount: 5,
      ),
    );
  }
}
