// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:otchet/ui/theme/input_formatters.dart';
// import 'package:otchet/ui/widgets/input_field_widget.dart';
//
// class IINField extends StatelessWidget {
//   final String hint;
//   final TextEditingController? iinEditingController;
//   const IINField({Key? key, required this.hint, this.iinEditingController})
//       : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     return CustomTextField(
//         headText: 'ИИН',
//         hintText: hint,
//         maxChar: 14,
//         inputType: TextInputType.number,
//         inputFormatter: [
//           FilteringTextInputFormatter.digitsOnly,
//           IINFormatter(),
//         ],
//         onChanged: (e) {
//           iin = e;
//         });
//   }
// }
