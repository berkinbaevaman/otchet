import 'package:flutter/material.dart';
import 'package:otchet/ui/theme/styles.dart';

class OutlinedActionButton extends StatefulWidget {
  final String label;
  final Function onPressed;

  const OutlinedActionButton(
      {Key? key, required this.label, required this.onPressed})
      : super(key: key);
  @override
  _OutlinedActionButtonState createState() => _OutlinedActionButtonState();
}

class _OutlinedActionButtonState extends State<OutlinedActionButton> {
  bool loading = false;
  double width = 200;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    WidgetsBinding.instance!.addPostFrameCallback((_) => change());
  }

  change() {
    setState(() {
      width = context.size!.width;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: GestureDetector(
        onTap: () async {
          setState(() {
            loading = true;
          });
          widget.onPressed();
          await Future.delayed(Duration(milliseconds: 300));

          setState(() {
            loading = false;
          });
        },
        child: Container(
          decoration: CustomTheme.shadowDecoration,
          width: MediaQuery.of(context).size.width - 50,
          height: 50,
          child: !loading
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(width: 16.0),
                    Icon(Icons.ac_unit),
                    Container(
                        padding: EdgeInsets.all(6.0),
                        width: width * 0.65,
                        child: Text(
                          widget.label,
                          style: TextStyle(color: CustomTheme.green),
                          textAlign: TextAlign.start,
                          maxLines: 2,
                        )),
                  ],
                )
              : Center(child: LoadingCircle(color: Colors.blue)),
        ),
      ),
    );
  }
}
