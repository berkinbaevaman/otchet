import 'package:flutter/material.dart';
import 'package:otchet/ui/theme/styles.dart';

class ActionButton extends StatefulWidget {
  final String label;
  final double fontSize;
  final Function onPressed;
  final IconData? icon;
  final backgroundColor;
  final padding;

  const ActionButton(
      {Key? key,
      required this.label,
      required this.onPressed,
      this.icon,
      this.backgroundColor,
      this.padding = const EdgeInsets.symmetric(vertical: 8.0),
      this.fontSize = 14})
      : super(key: key);
  @override
  _ActionButtonState createState() => _ActionButtonState();
}

class _ActionButtonState extends State<ActionButton> {
  bool loading = false;
  double width = 200;
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    WidgetsBinding.instance!.addPostFrameCallback((_) => change());
  }

  change() {
    setState(() {
      width = context.size!.width;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: widget.padding,
      child: GestureDetector(
        onTap: () async {
          setState(() {
            loading = true;
          });
          await Future.delayed(Duration(milliseconds: 250));

          setState(() {
            loading = false;
          });
          widget.onPressed();
        },
        child: Container(
          decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.all(Radius.circular(12))),
          // width: MediaQuery.of(context).size.width - 32,
          margin: EdgeInsets.symmetric(horizontal: 16),
          height: 50,
          child: Center(
            child: !loading
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      if (widget.icon != null)
                        Icon(widget.icon, color: Colors.white),
                      Container(
                          width: width * 0.5 + 20,
                          child: Text(
                            widget.label,
                            style: CustomTheme.buttonText
                                .copyWith(fontSize: widget.fontSize),
                            textAlign: TextAlign.center,
                            maxLines: 2,
                          )),
                    ],
                  )
                : LoadingCircle(),
          ),
        ),
      ),
    );
  }
}
