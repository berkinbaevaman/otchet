import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otchet/blocs/navigation_bloc/navigation_bloc.dart';
import 'package:otchet/ui/screens/cashier_screens/cash_main_page.dart';
import 'package:otchet/ui/screens/documents_screens/docs_main_page.dart';
import 'package:otchet/ui/screens/home/home_page.dart';
import 'package:otchet/ui/screens/profile_screens/profile_page.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/bars.dart';

const List<Widget> selected = [
  HomePage(),
  CashMainPage(),
  DocsMainPage(),
  ProfilePage()
];

class MainPage extends StatelessWidget {
  const MainPage();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => NavigationBloc(),
      child: Scaffold(
        // extendBodyBehindAppBar: true,
        // appBar: PreferredSize(
        //   preferredSize: Size.fromHeight(0),
        //   child: AppBar(
        //     elevation: 0.0,
        //     backgroundColor: Colors.transparent,
        //     brightness: Brightness.light,
        //   ),
        // ),
        body: SafeArea(
          child: BlocBuilder<NavigationBloc, UIState>(
              builder: (context, indexState) {
            return IndexedStack(
              index: indexState.index,
              children: selected,
            );
          }),
        ),
        bottomNavigationBar: CustomNavigationBar(),
      ),
    );
  }
}
