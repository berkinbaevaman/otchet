import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class NewsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0),
        child: AppBar(
          brightness: Brightness.dark,
          backgroundColor: Colors.black26,
          elevation: 0,
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            _NewsItem(),
            Image.asset('assets/news2.png'),
            SizedBox(
              height: 30,
            ),
            _NewsItem(),
          ],
        ),
      ),
    );
  }
}

class _NewsItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Image.asset('assets/news.png'),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                LightSubtitleText('23 января 2021'),
                SizedBox(
                  height: 10,
                ),
                TitleText(
                    'Porttitor rhoncus dolor purus non enim praesent elementum.'),
                Text(
                    'Ultricies integer quis auctor elit sed. Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus. Ante metus dictum at tempor.\n\n'
                    'Viverra mauris in aliquam sem fringilla ut morbi. Bibendum est ultricies integer quis auctor. Vestibulum rhoncus est pellentesque elit ullamcorper. Purus faucibus ornare suspendisse sed nisi lacus sed.'),
              ],
            ),
          )
        ],
      ),
    );
  }
}
