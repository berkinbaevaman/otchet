import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otchet/blocs/warehouse_bloc/warehous_state.dart';
import 'package:otchet/blocs/warehouse_bloc/warehouse_bloc.dart';
import 'package:otchet/services/utils_fucntions.dart';
import 'package:otchet/ui/screens/home/warehouse_screens/scan_page.dart';
import 'package:otchet/ui/screens/home/warehouse_screens/widgets/product_tile.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';
import 'package:otchet/ui/widgets/searchbar.dart';

class WarehousePage extends StatefulWidget {
  static const String title = 'Виртуальный склад';

  @override
  _WarehousePageState createState() => _WarehousePageState(title);
}

class _WarehousePageState extends State<WarehousePage> {
  final String title;
  _WarehousePageState(this.title);

  Widget _buildScanAction() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: InkWell(
          onTap: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => QRScanPage()));
          },
          child: Icon(Icons.qr_code_scanner_outlined)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return true;
      },
      child:
          BlocBuilder<WarehouseBloc, WarehouseState>(builder: (context, state) {
        print(state);
        return GestureDetector(
          onTap: () {
            hideKeyboard(context);
          },
          child: ProtoScaffold(
            appBarTitle: title,
            action: _buildScanAction(),
            body: (state is WarehouseLoadedState)
                ? SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SearchBar(),
                        Flexible(
                          flex: 1,
                          child: ListView.builder(
                              itemCount: state.wareList.length,
                              physics: NeverScrollableScrollPhysics(),

                              // physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemBuilder: (context, i) {
                                return ProductTile(
                                  item: state.wareList[i],
                                );
                              }),
                        )
                      ],
                    ),
                  )
                : Center(
                    child: CircularProgressIndicator.adaptive(),
                  ),
          ),
        );
      }),
    );
  }
}

//если не использовать этот класс то назад со свайпа на айос работать не будет
class CustomMaterialPageRoute extends MaterialPageRoute {
  @protected
  bool get hasScopedWillPopCallback {
    return false;
  }

  CustomMaterialPageRoute({
    required WidgetBuilder builder,
    RouteSettings? settings,
    bool maintainState = true,
    bool fullscreenDialog = false,
  }) : super(
          builder: builder,
          settings: settings,
          maintainState: maintainState,
          fullscreenDialog: fullscreenDialog,
        );
}
