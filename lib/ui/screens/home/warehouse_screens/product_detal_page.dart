import 'package:flutter/material.dart';
import 'package:otchet/models/ware_model.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class WareHouseItemDetails extends StatelessWidget {
  final Ware item;

  const WareHouseItemDetails({Key? key, required this.item}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ProtoScaffold(
        appBarTitle: '',
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 22.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 20,
                ),
                RegularText('${item.modelName}'),
                SizedBox(height: 20),
                //implements list view
                DetailTile('ШТРИХ КОД:', '${item.barcode}'),
                DetailTile('ЦЕНА:', '${item.price} Т'),
                DetailTile('КОЛИЧЕСТВО:', '${item.quantity} шт.'),
                DetailTile('БРЕНД:', '${item.brand}'),
                DetailTile('ЦВЕТ:', '${item.color}'),
              ],
            ),
          ),
        ));
  }
}

class DetailTile extends StatelessWidget {
  final String header;
  final String content;
  final double _padding = 14.0;

  const DetailTile(this.header, this.content);
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Divider(
          height: 1,
        ),
        SizedBox(
          height: _padding,
        ),
        LightSubtitleText(header),
        SizedBox(
          height: 16,
        ),
        RegularText(content),
        SizedBox(
          height: _padding,
        ),
        Divider(
          height: 1,
        ),
      ],
    );
  }
}
