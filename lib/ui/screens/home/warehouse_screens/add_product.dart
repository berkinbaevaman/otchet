import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otchet/blocs/warehouse_bloc/warehouse_bloc.dart';
import 'package:otchet/blocs/warehouse_bloc/warehouse_events.dart';
import 'package:otchet/models/ware_model.dart';
import 'package:otchet/services/utils_fucntions.dart';
import 'package:otchet/ui/screens/home/warehouse_screens/scan_page.dart';
import 'package:otchet/ui/widgets/buttons/action_button.dart';
import 'package:otchet/ui/widgets/input_field_widget.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';

class ItemAddingPage extends StatefulWidget {
  final String barcodeResult;

  const ItemAddingPage({required this.barcodeResult}) : super();
  @override
  _ItemAddingPageState createState() => _ItemAddingPageState();
}

class _ItemAddingPageState extends State<ItemAddingPage> {
  TextEditingController? barcode;
  String? modelName;
  int? price;
  int? quantity;
  String? brand;
  String? color;

  bool validate() {
    return modelName != null &&
        price != null &&
        quantity != null &&
        brand != null &&
        color != null;
  }

  @override
  initState() {
    barcode = TextEditingController(text: widget.barcodeResult);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        hideKeyboard(context);
      },
      child: ProtoScaffold(
        appBarTitle: 'Добавить товар',
        body: SingleChildScrollView(
          child: Column(
            children: [
              CustomTextField(
                headText: 'Штрих код',
                inputType: TextInputType.number,
                controller: barcode,
                hintText: '${widget.barcodeResult}',
                onIconPressed: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => QRScanPage()));
                },
                icon: Icons.qr_code_scanner_outlined,
              ),
              CustomTextField(
                headText: 'Название товара',
                hintText: 'Введите название товара',
                onChanged: (name) {
                  modelName = name;
                },
              ),
              CustomTextField(
                headText: 'Цена',
                hintText: '0 Т',
                inputType: TextInputType.number,
                onChanged: (priceS) {
                  price = int.parse(priceS);
                },
              ),
              CustomTextField(
                headText: 'Количество',
                hintText: '0 шт.',
                inputType: TextInputType.number,
                onChanged: (quantityS) {
                  quantity = int.parse(quantityS);
                },
              ),
              CustomTextField(
                headText: 'Бренд',
                hintText: 'BOSCH',
                onChanged: (brandS) {
                  brand = brandS;
                },
              ),
              CustomTextField(
                headText: 'Цвет',
                hintText: 'Черный',
                onChanged: (colorName) {
                  color = colorName;
                },
              ),
              SizedBox(
                height: 60,
              )
            ],
          ),
        ),
        navBar: Padding(
            padding: EdgeInsets.fromLTRB(20, 0, 20, 25),
            child: ActionButton(
                label: 'Добавить товар',
                onPressed: () async {
                  if (validate()) {
                    Ware newWare = Ware(
                        modelName: modelName!,
                        barcode: barcode!.text,
                        price: price!,
                        quantity: quantity!,
                        brand: brand!,
                        color: color!);
                    BlocProvider.of<WarehouseBloc>(context)
                        .add(AddWarehouseItemEvent(newWare));
                    await Future.delayed(Duration(milliseconds: 350));
                    Navigator.pop(context);
                  } else {
                    print('$modelName $price $quantity $brand $color');
                  }
                })),
      ),
    );
  }
}

bool checkNullFields(modelName, barcode, price, quantity, brand, color) {
  return modelName != null &&
      barcode != null &&
      price != null &&
      quantity != null &&
      brand != null &&
      color != null;
}
