import 'package:flutter/material.dart';
import 'package:otchet/models/ware_model.dart';
import 'package:otchet/ui/screens/home/warehouse_screens/product_detal_page.dart';
import 'package:otchet/ui/widgets/shadowed_container.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class ProductTile extends StatelessWidget {
  final Ware item;

  const ProductTile({Key? key, required this.item}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => WareHouseItemDetails(
                      item: item,
                    )));
      },
      child: ShadowedContainer(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              RegularText(item.modelName),
              Icon(Icons.arrow_forward_ios, size: 17),
            ],
          ),
          SizedBox(
            height: 19,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              RegularText('${item.price} тг'),
              RegularText(
                '${item.quantity} шт',
                align: TextAlign.center,
              ),
              Container(
                  width: 90,
                  color: Colors.black12,
                  child: Center(child: RegularText('${item.barcode}'))),
              SizedBox(
                width: 1,
              )
            ],
          )
        ],
      )),
    );
  }
}
