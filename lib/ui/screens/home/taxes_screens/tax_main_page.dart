import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:otchet/ui/screens/home/taxes_screens/page_view_screens/body_pages.dart';
import 'package:otchet/ui/screens/home/taxes_screens/page_view_screens/poping_navigation_screen.dart';
import 'package:otchet/ui/screens/home/taxes_screens/widgets/custom_dot_indicator.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';

class TaxPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ProtoScaffold(appBarTitle: 'Налоги', body: DottedPageView());
  }
}

class DottedPageView extends StatefulWidget {
  @override
  _DottedPageViewState createState() => _DottedPageViewState();
}

class _DottedPageViewState extends State<DottedPageView> {
  PageController _pageController = PageController();
  double currentPage = 0;

  @override
  void initState() {
    _pageController.addListener(() {
      setState(() {
        currentPage = _pageController.page!;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return NavigationScreen(_pageController);
              }));
            },
            child: Container(
              height: 65,
              width: MediaQuery.of(context).size.width,
              color: Colors.blue,
              child: Center(
                  child: Text(
                '${titles[currentPage.round()]}',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white),
              )),
            ),
          ),
          SizedBox(height: 16),
          DotsIndicator(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              onTap: (position) {
                _pageController.animateToPage(position.toInt(),
                    duration: Duration(milliseconds: 100),
                    curve: Curves.bounceIn);
                // setState((){
                //   currentPage = position;
                // });
              },
              decorator: DotsDecorator(
                color: Colors.grey, // Inactive color
                activeColor: Colors.blue,
              ),
              dotsCount: widgetList(_pageController).length,
              position: currentPage),
          Flexible(
            flex: 1,
            child: PageView(
              physics: NeverScrollableScrollPhysics(),
              controller: _pageController,
              children: widgetList(_pageController),
            ),
          )
        ],
      ),
    );
  }
}
