import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:otchet/ui/screens/home/taxes_screens/widgets/radio_widget_state.dart';
import 'package:otchet/ui/theme/input_formatters.dart';
import 'package:otchet/ui/widgets/input_field_widget.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

part 'pages_const.dart';

widgetList(controller) {
  final pages = [
    FirstSection(controller),
    SecondSection(controller),
    ThirdSection(controller),
    FourthSection(controller),
    FifthSection(controller),
    SixthSection(controller),
    SeventhSection(controller)
  ];
  return pages;
}

nextPage(controller) {
  controller.animateToPage((controller.page! + 1).round(),
      duration: Duration(milliseconds: 200), curve: Curves.decelerate);
}

class FirstSection extends StatelessWidget {
  final map = Map<String, dynamic>();
  final PageController controller;

  FirstSection(this.controller);
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomTextField(
            inputFormatter: [iinFormatter],
            headText: 'ИИН',
            hintText: 'XXXX-XXXX-XXXX',
          ),
          CustomTextField(
            // inputFormatter: [iinFormatter],
            headText: 'ФИО налогоплательщика',
            hintText: 'Пупкин В.В.',
          ),
          CustomTextField(
            // inputFormatter: [iinFormatter],
            headText:
                'Налоговый период, за который представляется налоговая отчетность:',
            hintText: '2020. г',
          ),
          DataFlexibleRadioList(
            titles: quarter,
            mapKey: 'quarter',
            map: map,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: LightSubtitleText('Отдельные категории налогоплательщика:'),
          ),
          DataFlexibleRadioList(
            titles: categories,
            mapKey: 'categories',
            map: map,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: LightSubtitleText('Вид декларации:'),
          ),
          DataFlexibleRadioList(
            titles: declaration,
            mapKey: 'declaration',
            map: map,
          ),
          CustomTextField(
            // inputFormatter: [iinFormatter],
            headText: 'Номер уведомления:',
            hintText: 'Введите номер уведомления',
          ),
          CustomTextField(
            // inputFormatter: [iinFormatter],
            headText: 'Дата уведомления:',
            hintText: 'ДД.ММ.ГГ',
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: LightSubtitleText('Признак резидентства:'),
          ),
          DataFlexibleRadioList(
            titles: rezident,
            mapKey: 'rezident',
            map: map,
          ),
          Center(
            child: ElevatedButton(
                onPressed: () {
                  nextPage(controller);
                },
                child: Text('Далее')),
          )
        ],
      ),
    );
  }
}

class SecondSection extends StatelessWidget {
  final PageController controller;
  SecondSection(this.controller);
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomTextField(
            inputFormatter: [iinFormatter],
            headText: 'ИИН',
            hintText: 'XXXX-XXXX-XXXX',
          ),
          CustomTextField(
            // inputFormatter: [iinFormatter],
            headText: 'ФИО налогоплательщика',
            hintText: 'Пупкин В.В.',
          ),
          CustomTextField(
            // inputFormatter: [iinFormatter],
            headText:
                'Налоговый период, за который представляется налоговая отчетность:',
            hintText: '2020. г',
          ),
          Center(
            child: ElevatedButton(
                onPressed: () {
                  nextPage(controller);
                },
                child: Text('Далее')),
          )
        ],
      ),
    );
  }
}

class ThirdSection extends StatelessWidget {
  final thirdSectionMap = Map<String, dynamic>();
  final PageController controller;
  ThirdSection(this.controller);
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
              itemCount: thirdContent.length,
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (context, index) {
                return SemesterContentBody(
                    index, thirdSectionMap, 'thirdSectionMap', thirdContent);
              }),
          Center(
            child: ElevatedButton(
                onPressed: () {
                  nextPage(controller);
                },
                child: Text('Далее')),
          )
        ],
      ),
    );
  }
}

class SemesterContentBody extends StatefulWidget {
  final sectionMap;
  final index;
  final mapKey;
  final content;
  SemesterContentBody(this.index, this.sectionMap, this.mapKey, this.content);
  @override
  _SemesterContentBodyState createState() => _SemesterContentBodyState();
}

class _SemesterContentBodyState extends State<SemesterContentBody> {
  bool show = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
            onTap: () {
              show = !show;
              setState(() {});
            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              child: Row(
                children: [
                  Expanded(
                      flex: 1,
                      child: RegularText(widget.content[widget.index])),
                  Icon(
                    show
                        ? Icons.arrow_downward_rounded
                        : Icons.arrow_forward_ios,
                    color: Colors.black,
                  ),
                ],
              ),
            )),
        if (show)
          SemesterTaxFields(
              widget.sectionMap, '${widget.mapKey}${widget.index}')
      ],
    );
  }
}

class FourthSection extends StatelessWidget {
  final fourthSectionMap = Map<String, dynamic>();
  final PageController controller;
  FourthSection(this.controller);
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ListView.builder(
              itemCount: fourthContent.length,
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemBuilder: (context, index) {
                return SemesterContentBody(
                    index, fourthSectionMap, 'fourthContent', fourthContent);
              }),
          Center(
            child: ElevatedButton(
                onPressed: () {
                  nextPage(controller);
                },
                child: Text('Далее')),
          )
        ],
      ),
    );
  }
}

class FifthSection extends StatelessWidget {
  final PageController controller;
  FifthSection(this.controller);
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: RegularText('Запасы:'),
          ),
          CustomTextField(
            inputFormatter: [iinFormatter],
            headText: 'Запасы на начало налогового периода всего:',
            hintText: '0 ₸',
          ),
          CustomTextField(
            // inputFormatter: [iinFormatter],
            headText: 'Запасы на конец налогового периода всего:',
            hintText: '0 ₸',
          ),
          CustomTextField(
            // inputFormatter: [iinFormatter],
            headText: 'Приобретено запасов, работ, услуг всего:',
            hintText: '0 ₸',
          ),
          Center(
            child: ElevatedButton(
                onPressed: () {
                  nextPage(controller);
                },
                child: Text('Далее')),
          )
        ],
      ),
    );
  }
}

class SixthSection extends StatelessWidget {
  final PageController controller;

  SixthSection(this.controller);
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: RegularText(
              'Бизнес - идентификационный номер аппарата акимов городов районного значения, сел, поселков, сельских округов по месту нахождения индивидуального предпринимателя:',
            ),
          ),
          CustomTextField(
            inputFormatter: [iinFormatter],
            hintText: 'Введите идентификационный номер ',
          ),
          Center(
            child: ElevatedButton(
                onPressed: () {
                  nextPage(controller);
                },
                child: Text('Далее')),
          )
        ],
      ),
    );
  }
}

class SeventhSection extends StatelessWidget {
  final controller;
  SeventhSection(this.controller);
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomTextField(
            inputFormatter: [iinFormatter],
            headText: 'ФИО налогоплательщика(руководителя):',
            hintText: 'Арманов Ильяс Сапарулы',
          ),
          CustomTextField(
            // inputFormatter: [iinFormatter],
            headText: 'Дата подачи декларации:',
            hintText: 'ДД.ММ.ГГ',
          ),
          CustomTextField(
            // inputFormatter: [iinFormatter],
            headText: 'Код органа государственных доходов по месту нахождения:',
            hintText: '0000',
          ),
          CustomTextField(
            // inputFormatter: [iinFormatter],
            headText: 'Код органа государственных доходов по месту жительства:',
            hintText: '0000',
          ),
          Center(
            child: ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                  // controller.animateToPage((controller.page! + 1).round(),
                  //     duration: Duration(milliseconds: 200),
                  //     curve: Curves.decelerate);
                },
                child: Text('Готово')),
          )
        ],
      ),
    );
  }
}

class SemesterTaxFields extends StatelessWidget {
  final Map<String, dynamic> map;
  final String mapKey;
  SemesterTaxFields(this.map, this.mapKey);
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: 5,
        itemBuilder: (context, index) {
          return CustomTextField(
            hintText: '0 ₸',
            headText: '${(index + 1)} месяц',
            onChanged: (String value) {
              map['$mapKey${(index + 1)}'] = value;
              print(map);
            },
          );
        });
  }
}
