import 'package:flutter/material.dart';
import 'package:otchet/ui/screens/home/taxes_screens/page_view_screens/body_pages.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

// class NavigationScreens extends StatefulWidget {
//   @override
//   _NavigationScreenState createState() => _NavigationScreenState();
// }
//
// class _NavigationScreenStates extends State<NavigationScreens> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: SafeArea(
//         child: ListView.builder(
//             padding: EdgeInsets.all(16),
//             itemCount: titles.length,
//             shrinkWrap: true,
//             itemBuilder: (context, index) {
//               return Padding(
//                 padding: const EdgeInsets.symmetric(vertical: 8.0),
//                 child: GestureDetector(
//                     onTap: () {
//                       widget.controller.animateToPage(index,
//                           duration: Duration(milliseconds: 200),
//                           curve: Curves.bounceInOut);
//                     },
//                     child: RegularText(titles[index])),
//               );
//             }),
//       ),
//     );
//   }
// }

class NavigationScreen extends StatelessWidget {
  final PageController controller;
  NavigationScreen(this.controller);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ListView.builder(
            padding: EdgeInsets.all(16),
            itemCount: titles.length,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: GestureDetector(
                    onTap: () {
                      controller.animateToPage(index,
                          duration: Duration(milliseconds: 200),
                          curve: Curves.bounceInOut);
                      Navigator.pop(context);
                    },
                    child: RegularText(titles[index])),
              );
            }),
      ),
    );
  }
}
