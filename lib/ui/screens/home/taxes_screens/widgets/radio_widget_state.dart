import 'package:flutter/material.dart';

class DataFlexibleRadioList extends StatefulWidget {
  final List<String> titles;
  final Map<String, dynamic> map;
  final String mapKey;

  const DataFlexibleRadioList(
      {required this.mapKey, required this.titles, required this.map})
      : super();
  @override
  _DataFlexibleRadioListState createState() => _DataFlexibleRadioListState();
}

class _DataFlexibleRadioListState extends State<DataFlexibleRadioList> {
  int current = 0;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        itemCount: widget.titles.length,
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              widget.map['${widget.mapKey}'] = widget.titles[index];
              setState(() {
                current = index;
              });
            },
            child: Row(
              children: [
                Container(
                  child: Container(
                    margin: EdgeInsets.all(4),
                    // width: 12,
                    // height: 12,
                    // margin: decorator.spacing,
                    decoration: ShapeDecoration(
                      color: current == index ? Colors.blue : null,
                      shape: CircleBorder(),
                    ),
                  ),
                  width: 20,
                  height: 20,
                  // margin: decorator.spacing,
                  decoration: ShapeDecoration(
                    color: Colors.grey.shade400.withOpacity(0.5),
                    shape: CircleBorder(),
                  ),
                ),
                SizedBox(width: 6),
                Expanded(flex: 1, child: Text(widget.titles[index]))
              ],
            ),
          );
        });
  }
}
