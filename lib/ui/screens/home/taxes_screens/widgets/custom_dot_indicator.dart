import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DotsIndicator extends StatelessWidget {
  final int dotsCount;
  final double position;
  final DotsDecorator decorator;
  final onTap;
  final MainAxisSize mainAxisSize;
  final MainAxisAlignment mainAxisAlignment;

  const DotsIndicator({
    Key? key,
    required this.dotsCount,
    this.position = 0.0,
    this.decorator = const DotsDecorator(),
    this.mainAxisSize = MainAxisSize.min,
    this.mainAxisAlignment = MainAxisAlignment.center,
    this.onTap,
  })  : assert(dotsCount > 0),
        assert(position >= 0),
        assert(
          position < dotsCount,
          "Position must be inferior than dotsCount",
        ),
        super(key: key);

  Widget _buildDot(int index) {
    Color? color =
        position.round() >= index ? decorator.activeColor : decorator.color;
    final lined = Row(
      // mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          height: 2,
          width: 40,
          color: color,
        ),
        Container(
          // decoration: ,
          decoration: position.round() == index
              ? BoxDecoration(
                  border: Border.all(color: Colors.blueAccent),
                  borderRadius:
                      new BorderRadius.all(const Radius.circular(40.0)))
              : null,
          child: Container(
            width: decorator.size.width,
            height: decorator.size.height,
            margin: decorator.spacing,
            decoration: ShapeDecoration(
              color: color,
              shape: decorator.shape,
            ),
          ),
        )
      ],
    );

    final dot = Container(
      decoration: position.round() == index
          ? BoxDecoration(
              border: Border.all(color: Colors.blueAccent),
              borderRadius: new BorderRadius.all(const Radius.circular(40.0)))
          : null,
      child: Container(
        width: decorator.size.width,
        height: decorator.size.height,
        margin: decorator.spacing,
        decoration: ShapeDecoration(
          color: color,
          shape: decorator.shape,
        ),
      ),
    );
    return onTap == null
        ? (index == 0 ? dot : lined)
        : InkWell(
            customBorder: const CircleBorder(),
            onTap: () => onTap!(index.toDouble()),
            child: index == 0 ? dot : lined,
          );
  }

  @override
  Widget build(BuildContext context) {
    final dotsList = List<Widget>.generate(dotsCount, _buildDot);

    return Row(
      mainAxisAlignment: mainAxisAlignment,
      mainAxisSize: mainAxisSize,
      children: dotsList,
    );
  }
}

const Size kDefaultSize = Size.square(13.0);
const EdgeInsets kDefaultSpacing = EdgeInsets.all(6.0);
const ShapeBorder kDefaultShape = CircleBorder();

class DotsDecorator {
  /// Inactive dot color
  ///
  /// @Default `Colors.grey`
  final Color color;

  /// Active dot color
  ///
  /// @Default `Colors.lightBlue`
  final Color activeColor;

  /// Inactive dot size
  ///
  /// @Default `Size.square(9.0)`
  final Size size;

  /// Active dot size
  ///
  /// @Default `Size.square(9.0)`
  final Size activeSize;

  /// Inactive dot shape
  ///
  /// @Default `CircleBorder()`
  final ShapeBorder shape;

  /// Active dot shape
  ///
  /// @Default `CircleBorder()`
  final ShapeBorder activeShape;

  /// Spacing between dots
  ///
  /// @Default `EdgeInsets.all(6.0)`
  final EdgeInsets spacing;

  const DotsDecorator({
    this.color = Colors.grey,
    this.activeColor = Colors.lightBlue,
    this.size = kDefaultSize,
    this.activeSize = kDefaultSize,
    this.shape = kDefaultShape,
    this.activeShape = kDefaultShape,
    this.spacing = kDefaultSpacing,
  });
}
