import 'package:flutter/material.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class FAQ extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ProtoScaffold(
      appBarTitle: '',
      body: SingleChildScrollView(
        child: Column(
          children: [
            ListView.builder(
                itemCount: 2,
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (context, i) {
                  return Padding(
                    padding: const EdgeInsets.only(bottom: 18.0),
                    child: _FAQItem(
                      img: (i + 1),
                    ),
                  );
                }),
            SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }
}

class _FAQItem extends StatelessWidget {
  final img;

  const _FAQItem({Key? key, this.img}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TitleText(
                    'Porttitor rhoncus dolor purus non enim praesent elementum.'),
                SizedBox(
                  height: 10,
                ),
                Text(
                    'Ultricies integer quis auctor elit sed. Amet nulla facilisi morbi tempus iaculis urna id volutpat lacus. Ante metus dictum at tempor.\n\n'
                    'Viverra mauris in aliquam sem fringilla ut morbi. Bibendum est ultricies integer quis auctor. Vestibulum rhoncus est pellentesque elit ullamcorper. Purus faucibus ornare suspendisse sed nisi lacus sed.'),
              ],
            ),
          ),
          Image.asset('assets/faq$img.png'),
          LightSubtitleText('23 января 2021'),
        ],
      ),
    );
  }
}
