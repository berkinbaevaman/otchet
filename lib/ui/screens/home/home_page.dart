import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otchet/blocs/employee_bloc/employee_bloc.dart';
import 'package:otchet/blocs/employee_bloc/employee_events.dart';
import 'package:otchet/blocs/warehouse_bloc/warehouse_bloc.dart';
import 'package:otchet/blocs/warehouse_bloc/warehouse_events.dart';
import 'package:otchet/ui/screens/home/employee_screens/employee_main_page.dart';
import 'package:otchet/ui/screens/home/faq/faq.dart';
import 'package:otchet/ui/screens/home/news/news_main_page.dart';
import 'package:otchet/ui/screens/home/taxes_screens/tax_main_page.dart';
import 'package:otchet/ui/screens/home/warehouse_screens/warehouse_main_page.dart';
import 'package:otchet/ui/theme/styles.dart';
import 'package:otchet/ui/widgets/news_scrolling_tab.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/bars.dart';
import 'package:otchet/ui/widgets/shadowed_container.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class HomePage extends StatelessWidget {
  const HomePage();

  @override
  Widget build(BuildContext context) {
    final employeesBloc = BlocProvider.of<EmployeeBloc>(context);
    final waresBloc = BlocProvider.of<WarehouseBloc>(context);
    double width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: SingleChildScrollView(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: 16,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16),
            child: Text(
              'Главная',
              style: TextStyle(fontSize: 34, fontWeight: FontWeight.w700),
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Container(
            width: width,
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: InkWell(
                    onTap: () {
                      dialogSoon(context);

                      // employeesBloc.add(LoadEmployeesEvent());
                      // Navigator.push(
                      //     context,
                      //     MaterialPageRoute(
                      //         builder: (context) => EmployeesMainPage()));
                    },
                    child: Image.asset('assets/employess.png'),
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
                Flexible(
                  child: InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => TaxPage()));
                    },
                    child: Image.asset('assets/tax.png'),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
          Container(),
          Flexible(
            fit: FlexFit.loose,
            child: InkWell(
              onTap: () {
                dialogSoon(context);

                // Navigator.push(
                //     context,
                //     CustomMaterialPageRoute(
                //         builder: (context) => WarehousePage()));
                // waresBloc.add(LoadWarehouseEvent());
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Image.asset('assets/warehouse.png'),
              ),
            ),
          ),
          SizedBox(
            height: 16,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: GestureDetector(
              onTap: () {
                // dialogSoon(context);

                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => NewsPage()));
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TitleText('Новости'),
                  Text(
                    'Все',
                    style: TextStyle(color: Colors.blue),
                  ),
                ],
              ),
            ),
          ),
          NewsTab(),
          Padding(
            padding: const EdgeInsets.fromLTRB(16, 16, 16, 1),
            child: TitleText(
              'FAQ',
            ),
          ),
          ListView.builder(
              itemCount: 4,
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return InkWell(
                  onTap: () {
                    // dialogSoon(context);

                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => FAQ()));
                  },
                  child: ShadowedContainer(
                      child: Row(
                    children: [
                      Text(
                        'Tellus orci ac auctor augue mauris?',
                        textAlign: TextAlign.left,
                        style: TextStyle(),
                      ),
                    ],
                  )),
                );
              })
        ],
      )),
    );
  }
}
