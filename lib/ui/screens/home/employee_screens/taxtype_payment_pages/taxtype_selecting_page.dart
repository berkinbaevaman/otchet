import 'package:flutter/material.dart';
import 'package:otchet/ui/screens/home/employee_screens/taxtype_payment_pages/payment_page.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class PayEmployeeTaxesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ProtoScaffold(
        appBarTitle: 'Оплатить налоги',
        body: Column(
          children: ListTile.divideTiles(context: context, tiles: [
            ListTile(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PaymentPage()));
              },
              title: Container(
                  child: RegularText('Обязательные пенсионные взносы')),
              trailing: Icon(
                Icons.arrow_forward_ios,
                size: 18,
              ),
            ),
            ListTile(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PaymentPage()));
              },
              title: RegularText('Индивидуальный подоходный налог'),
              trailing: Icon(
                Icons.arrow_forward_ios,
                size: 18,
              ),
            ),
            ListTile(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PaymentPage()));
              },
              title: RegularText('Социальные отчисления'),
              trailing: Icon(
                Icons.arrow_forward_ios,
                size: 18,
              ),
            ),
            ListTile(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PaymentPage()));
              },
              title: RegularText(
                  'Отчисления на обязательное соц. мед. страхование'),
              trailing: Icon(
                Icons.arrow_forward_ios,
                size: 18,
              ),
            ),
            ListTile(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PaymentPage()));
              },
              title:
                  RegularText('Взносы на обязательное соц. мед. страхование'),
              trailing: Icon(
                Icons.arrow_forward_ios,
                size: 18,
              ),
            ),
          ]).toList(),
        ));
  }
}
