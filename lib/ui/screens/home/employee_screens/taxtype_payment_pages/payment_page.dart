import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:otchet/ui/widgets/buttons/action_button.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class PaymentPage extends StatefulWidget {
  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  Widget pad = SizedBox(
    height: 16,
    width: 16,
  );

  List<String?> names = ['Тип платежа', 'molo', 'solo'];
  Widget datePicker() {
    return Container(
      // width: width,
      height: 180,
      child: Center(
        child: CupertinoDatePicker(
            mode: CupertinoDatePickerMode.date,
            minimumYear: 2000,
            maximumYear: 2022,
            minimumDate: DateTime(2000),
            maximumDate: DateTime(2022),
            initialDateTime: DateTime.now(),
            onDateTimeChanged: (_) {}),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ProtoScaffold(
      appBarTitle: 'Название типа платежа',
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 22.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              DropdownButton<String>(
                isExpanded: true,
                underline: Container(),
                hint: Text(''),
                value: names[0],
                icon: Container(
                  padding: EdgeInsets.only(right: 10),
                  child: Icon(
                    Icons.keyboard_arrow_down,
                    color: Colors.grey,
                  ),
                ),
                iconSize: 24,
                elevation: 0,
                style: TextStyle(color: Colors.black),
                onChanged: (String? newValue) {
                  // var index = names.indexOf(newValue);
                },
                items: names.map<DropdownMenuItem<String>>((value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Container(
                        padding: EdgeInsets.only(left: 10),
                        child: RegularText(value ?? '')),
                  );
                }).toList(),
              ),
              datePicker(),
              pad,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('ИИН плательщика'),
                  RegularText('2911-8238-1843'),
                ],
              ),
              pad,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('ИИН работника'),
                  RegularText('0124-]235-9781'),
                ],
              ),
              pad,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Сумма оплаты'),
                  RegularText('50 000 т'),
                ],
              ),
              pad,
            ],
          ),
        ),
      ),
      navBar: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 20),
        child: ActionButton(label: 'Оплатить', onPressed: () {}),
      ),
    );
  }
}
