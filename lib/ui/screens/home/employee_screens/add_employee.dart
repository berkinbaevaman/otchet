import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otchet/blocs/employee_bloc/employee_bloc.dart';
import 'package:otchet/blocs/employee_bloc/employee_events.dart';
import 'package:otchet/models/employee_model.dart';
import 'package:otchet/ui/theme/input_formatters.dart';
import 'package:otchet/ui/widgets/buttons/action_button.dart';
import 'package:otchet/ui/widgets/input_field_widget.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';

class AddEmployeePage extends StatefulWidget {
  static const String title = 'Добавить сотрудника';

  @override
  _AddEmployeePageState createState() => _AddEmployeePageState();
}

class _AddEmployeePageState extends State<AddEmployeePage> {
  late EmployeeBloc bloc;
  String name = 'POP MOP BOB';
  String iin = 'XXXX XXXX XXXX';
  String salary = '250 000';

  @override
  void initState() {
    bloc = BlocProvider.of<EmployeeBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ProtoScaffold(
        appBarTitle: AddEmployeePage.title,
        body: Column(
          children: [
            CustomTextField(
                headText: 'ФИО',
                hintText: name,
                onChanged: (e) {
                  name = e;
                }),
            CustomTextField(
                headText: 'ИИН',
                hintText: iin,
                maxChar: 14,
                inputType: TextInputType.number,
                inputFormatter: [
                  FilteringTextInputFormatter.digitsOnly,
                  iinFormatter,
                ],
                onChanged: (e) {
                  iin = e;
                }),
            CustomTextField(
                headText: 'Заработная плата',
                hintText: salary,
                maxChar: 14,
                inputFormatter: [
                  FilteringTextInputFormatter.digitsOnly,
                  SalaryInputFormatter(),
                ],
                inputType: TextInputType.number,
                onChanged: (e) {
                  salary = e;
                }),
            Center(
              child: ActionButton(
                  label: 'Добавить',
                  onPressed: () async {
                    bloc.add(AddNewEmployeeEvent(Employee(name, salary, iin)));
                    await Future.delayed(Duration(milliseconds: 400))
                        .whenComplete(() => Navigator.pop(context));
                  }),
            )
          ],
        ));
  }
}
//            CustomTextField(
//                 headText: 'Пароль сотрудника',
//                 hintText: '123457890',
//                 onChanged: (e){},
//                 obscure: true,
//             ),
