import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otchet/blocs/employee_bloc/employee_bloc.dart';
import 'package:otchet/blocs/employee_bloc/employee_state.dart';
import 'package:otchet/ui/screens/home/employee_screens/add_employee.dart';
import 'package:otchet/ui/screens/home/employee_screens/employee_details.dart';
import 'package:otchet/ui/widgets/buttons/action_button.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';
import 'package:otchet/ui/widgets/shadowed_container.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class EmployeesMainPage extends StatefulWidget {
  static String title = 'Сотрудники';

  @override
  _EmployeesMainPageState createState() => _EmployeesMainPageState();
}

class _EmployeesMainPageState extends State<EmployeesMainPage> {
  @override
  Widget build(BuildContext context) {
    return ProtoScaffold(
      appBarTitle: EmployeesMainPage.title,
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: [
              ActionButton(
                // padding: EdgeInsets.fromLTRB(12.0, 16.0, 12.0, 8.0),
                icon: Icons.person_add_alt_1_outlined,
                label: 'Добавить сотрудника',
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AddEmployeePage()));
                },
              ),
              BlocBuilder<EmployeeBloc, EmployeeState>(
                  builder: (context, state) {
                print('rebuild works');
                if (state is EmployeeLoadingState) {
                  return Container(
                    height: MediaQuery.of(context).size.height - 300,
                    child: Center(child: CircularProgressIndicator()),
                  );
                } else if (state is EmployeesDataState) {
                  return ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: state.employees.length,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return _EmployeesCard(
                          dataState: state,
                          index: index,
                        );
                      });
                } else if (state is EmployeeErrorState) {
                  return Center(child: Text('error'));
                } else {
                  return Container(
                    height: MediaQuery.of(context).size.height - 300,
                    child: Center(child: CircularProgressIndicator()),
                  );
                }
              }),
            ],
          ),
        ),
      ),
    );
  }
}

class _EmployeesCard extends StatelessWidget {
  final int index;
  final EmployeesDataState dataState;
  const _EmployeesCard({Key? key, required this.index, required this.dataState})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => EmployeeInfoPage(
                      index: index,
                    )));
      },
      child: ShadowedContainer(
          padding: EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  RegularText(dataState.employees[index].name),
                  Icon(
                    Icons.arrow_forward_ios_outlined,
                    size: 14,
                  )
                ],
              ),
              SizedBox(
                height: 6,
              ),
              Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SubtitleText('ИИН'),
                      SizedBox(
                        height: 6,
                      ),
                      RegularText(
                          dataState.employees[index].identificationNumber),
                    ],
                  ),
                  SizedBox(width: 75.0),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SubtitleText('Заработная плата'),
                      SizedBox(
                        height: 6,
                      ),
                      RegularText(dataState.employees[index].salary),
                    ],
                  )
                ],
              ),
            ],
          )),
    );
  }
}
