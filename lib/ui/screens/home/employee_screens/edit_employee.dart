import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otchet/blocs/employee_bloc/employee_bloc.dart';
import 'package:otchet/blocs/employee_bloc/employee_events.dart';
import 'package:otchet/models/employee_model.dart';
import 'package:otchet/ui/theme/input_formatters.dart';
import 'package:otchet/ui/widgets/input_field_widget.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class EditEmployeePage extends StatefulWidget {
  final Employee employee;
  final int index;

  EditEmployeePage(this.employee, this.index);

  static const String title = 'Редактировать сотрудника';

  @override
  _EditEmployeePageState createState() => _EditEmployeePageState();
}

class _EditEmployeePageState extends State<EditEmployeePage> {
  Employee newEmployee = Employee('name', 'salary', 'identificationNumber');
  late EmployeeBloc bloc;
  late TextEditingController nameController;
  late TextEditingController salaryController;
  late TextEditingController iinController;

  _appBar(context) {
    return AppBar(
      elevation: 0.0,
      leadingWidth: 80,
      leading: GestureDetector(
        onTap: () {
          Navigator.pop(context);
        },
        child: Padding(
          padding: const EdgeInsets.only(left: 8.0, top: 8.0),
          child: Center(
              child: ColoredButtonText(text: 'Отмена', color: Colors.blue)),
        ),
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: Center(
              child: InkWell(
                  onTap: () async {
                    var name = nameController.text;
                    var salary = salaryController.text;
                    var iin = iinController.text;
                    Employee newWorker = Employee(name, salary, iin);
                    bloc.add(EditEmployeeEvent(widget.employee, newWorker));
                    await Future.delayed(Duration(milliseconds: 400));
                    Navigator.pop(context);
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child:
                        ColoredButtonText(text: 'Готово', color: Colors.blue),
                  ))),
        ),
        SizedBox(
          width: 10,
        )
      ],
    );
  }

  @override
  initState() {
    super.initState();
    bloc = BlocProvider.of<EmployeeBloc>(context);
    nameController = TextEditingController(text: widget.employee.name);
    salaryController = TextEditingController(text: widget.employee.salary);
    iinController =
        TextEditingController(text: widget.employee.identificationNumber);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: _appBar(context),
        body: Column(
          children: [
            CustomTextField(
              headText: 'ФИО',
              controller: nameController,
              hintText: widget.employee.name,
            ),
            CustomTextField(
              headText: 'ИИН',
              hintText: '${widget.employee.identificationNumber}',
              controller: iinController,
              maxChar: 14,
              inputType: TextInputType.number,
              inputFormatter: [
                FilteringTextInputFormatter.digitsOnly,
                iinFormatter,
              ],
            ),
            CustomTextField(
              headText: 'Заработная плата',
              hintText: '${widget.employee.salary}',
              controller: salaryController,
              maxChar: 14,
              inputFormatter: [
                FilteringTextInputFormatter.digitsOnly,
                SalaryInputFormatter(),
              ],
              inputType: TextInputType.number,
            ),
          ],
        ));
  }
}
