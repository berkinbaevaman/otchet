import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class AccessesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ProtoScaffold(
      appBarTitle: 'Доступ',
      body: Padding(
        padding: const EdgeInsets.all(18.0),
        child: Column(
          children: [
            Text(
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
            AccessItem('Taxes', false),
            AccessItem('Warehouse', false),
            AccessItem('Cahier', false),
            AccessItem('Docs', false),
          ],
        ),
      ),
    );
  }
}

class AccessItem extends StatefulWidget {
  final String text;
  final bool value;
  AccessItem(this.text, this.value) : super();
  @override
  _AccessItemState createState() => _AccessItemState(text: text, value: value);
}

class _AccessItemState extends State<AccessItem> {
  final String text;
  bool value;

  _AccessItemState({required this.text, required this.value}) : super();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          RegularText(text),
          CupertinoSwitch(
              value: value,
              onChanged: (_) {
                setState(() {
                  value = !value;
                });
              })
        ],
      ),
    );
  }
}
