import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otchet/blocs/employee_bloc/employee_bloc.dart';
import 'package:otchet/blocs/employee_bloc/employee_state.dart';
import 'package:otchet/ui/screens/home/employee_screens/accesses.dart';
import 'package:otchet/ui/screens/home/employee_screens/edit_employee.dart';
import 'package:otchet/ui/screens/home/employee_screens/taxtype_payment_pages/taxtype_selecting_page.dart';
import 'package:otchet/ui/widgets/buttons/outlined_green_button.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class EmployeeInfoPage extends StatelessWidget {
  static const String title = 'Редактировать сотрудника';
  final int index;

  const EmployeeInfoPage({Key? key, required this.index}) : super(key: key);

  Widget _buildAction(employee, index, context) {
    return Row(
      children: [
        InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => EditEmployeePage(employee, index)));
            },
            child:
                Padding(padding: EdgeInsets.all(10), child: Icon(Icons.edit))),
        SizedBox(width: 5)
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EmployeeBloc, EmployeeState>(builder: (context, state) {
      var employees = [];
      if (state is EmployeesDataState) employees = state.employees;
      return ProtoScaffold(
          appBarTitle: '',
          action: _buildAction(employees[index], index, context),
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 26.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TitleText(employees[index].name),
                SizedBox(
                  height: 8,
                ),
                SubtitleText('ИИН'),
                SizedBox(
                  height: 8,
                ),
                RegularText(employees[index].identificationNumber),
                SizedBox(
                  height: 8,
                ),
                SubtitleText('Заработная плата'),
                SizedBox(
                  height: 8,
                ),
                RegularText(employees[index].salary),
                SizedBox(
                  height: 8,
                ),
                SubtitleText('Пароль'),
                SizedBox(
                  height: 8,
                ),
                RegularText('*********'),
                SizedBox(
                  height: 8,
                ),
                Center(
                  child: OutlinedActionButton(
                      label: 'Оплатить налоги',
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => PayEmployeeTaxesPage()));
                      }),
                ),
                Center(
                  child: OutlinedActionButton(
                      label: 'Доступ',
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AccessesPage()));
                      }),
                )
              ],
            ),
          ));
    });
  }
}
