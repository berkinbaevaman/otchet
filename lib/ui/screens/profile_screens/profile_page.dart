import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/bars.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
          child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(height: 16),
            LightHeaderText('Наименование организации:'),
            Padding(
              padding: const EdgeInsets.only(top: 10.0, bottom: 20),
              child: TitleText(
                'ИП “Васильев В. В.”',
                // style: TextStyle(fontSize: 34, fontWeight: FontWeight.w700),
              ),
            ),
            LightHeaderText('БИН:'),
            SizedBox(
              height: 9,
            ),
            TitleText('880304502010'),
            SizedBox(height: 40),
            Flexible(
              flex: 1,
              child: ListView.separated(
                padding: EdgeInsets.zero,
                itemCount: _profileTileNames.length,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return ListTile(
                    onTap: () {
                      if (index == 0)
                        dialogSoon(context);
                      else
                        Navigator.pushNamed(context, _profileTileNames[index]);
                    },
                    contentPadding: EdgeInsets.zero,
                    leading: Icon(
                      leading[index],
                      color: Colors.blue,
                    ),
                    title: Text(_profileTileNames[index]),
                    trailing: Icon(
                      CupertinoIcons.forward,
                    ),
                  );
                },
                separatorBuilder: (context, index) {
                  return Divider(
                    color: Colors.grey.withOpacity(0.5),
                  );
                },
              ),
            )
          ],
        ),
      )),
    );
  }
}

const List<IconData> leading = [
  Icons.shop_outlined,
  Icons.credit_card_outlined,
  Icons.language,
  Icons.notifications_none_outlined
];

const List<String> _profileTileNames = [
  'Магазины',
  'Реквизиты',
  'Язык',
  'Уведомления'
];

class _ProfileTiles extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
