import 'package:flutter/material.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class LocalizationPage extends StatelessWidget {
  static var title = 'Язык';
  @override
  Widget build(BuildContext context) {
    return ProtoScaffold(
      appBarTitle: 'Язык',
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                RegularText('Казахский'),
                // Icon(
                //   Icons.done,
                //   color: Colors.blue,
                // )
              ],
            ),
            SizedBox(
              height: 29,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                RegularText('Русский'),
                Icon(
                  Icons.done,
                  color: Colors.blue,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
