import 'package:flutter/material.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class CredentialsPage extends StatelessWidget {
  static var title = 'Реквизиты';
  @override
  Widget build(BuildContext context) {
    return ProtoScaffold(
      appBarTitle: 'Реквизиты',
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SubtitleText('БАНК'),
            SizedBox(
              height: 4,
            ),
            RegularText('ДБ АО Сбербанк'),
            SizedBox(
              height: 8,
            ),
            SubtitleText('ИИК'),
            SizedBox(
              height: 4,
            ),
            RegularText('KZ298548789101212333'),
            SizedBox(
              height: 8,
            ),
            SubtitleText('БИК'),
            SizedBox(
              height: 4,
            ),
            RegularText('KZSBRT'),
          ],
        ),
      ),
    );
  }
}
