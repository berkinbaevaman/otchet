import 'package:flutter/material.dart';
import 'package:otchet/ui/widgets/input_field_widget.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';

class AddShopPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ProtoScaffold(
        appBarTitle: 'Добавить магазин',
        body: Column(
          children: [
            CustomTextField(
              hintText: 'Наименование магазина',
              headText: 'Наименование магазина',
            ),
            CustomTextField(
              hintText: 'ХХХХХХХХХХХХХХХХ',
              headText: 'Заводской номер кассового аппарата',
            ),
            CustomTextField(
              hintText: 'Введите токен',
              headText: 'Модель кассового аппарта',
            ),
            CustomTextField(
              hintText: '2020г',
              headText: 'Год выпуска',
            ),
            CustomTextField(
              hintText: 'Введите токен',
              headText: 'Токен',
            ),
            CustomTextField(
              hintText: 'Введите ID',
              headText: 'ID',
            ),
            CustomTextField(
              hintText: 'Введите регистрационный номер',
              headText: 'Регистрационный номер',
            ),
            CustomTextField(hintText: 'Введите адрес', headText: 'Адрес'),
          ],
        ));
  }
}
