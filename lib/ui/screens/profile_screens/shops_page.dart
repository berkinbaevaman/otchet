import 'package:flutter/material.dart';
import 'package:otchet/ui/screens/profile_screens/add_shop.dart';
import 'package:otchet/ui/widgets/buttons/action_button.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class ShopList extends StatelessWidget {
  static var title = 'Магазины';
  @override
  Widget build(BuildContext context) {
    return ProtoScaffold(
      appBarTitle: 'Магазины',
      body: Column(
        children: [
          ListView.builder(
              shrinkWrap: true,
              padding: EdgeInsets.symmetric(horizontal: 16),
              itemCount: _shopTiles.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.symmetric(vertical: 16.0),
                  child: RegularText(_shopTiles[index]),
                );
              })
        ],
      ),
      navBar: Padding(
        padding: const EdgeInsets.only(bottom: 32.0),
        child: ActionButton(
            label: 'Добавить',
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return AddShopPage();
              }));
            }),
      ),
    );
  }
}

List<String> _shopTiles = ['"Светлана"', '"Electric"', '"Melectric"'];
