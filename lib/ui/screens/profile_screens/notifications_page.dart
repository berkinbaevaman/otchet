import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class NotificationPage extends StatelessWidget {
  final List asd = [];
  static var title = 'Уведомления';
  @override
  Widget build(BuildContext context) {
    return ProtoScaffold(
      appBarTitle: 'Уведомления',
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Image.asset(
              'assets/notification.png',
              scale: 2,
            ),
            Text(
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt'),
            // Switcher(
            //   title: 'Закрытие кассы',
            // ),
            Switcher(
              title: 'Оплата налога',
            ),
            Switcher(
              title: 'Разрешить уведомления',
            )
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //   children: [
            //     Text('asd'),
            //     CupertinoSwitch(
            //         value: false,
            //         onChanged: (boole) {
            //           // yes = !yes;
            //         }),
            //   ],
            // )
          ],
        ),
      ),
    );
  }
}

class Switcher extends StatefulWidget {
  final String title;

  const Switcher({Key? key, required this.title}) : super(key: key);
  @override
  _SwitcherState createState() => _SwitcherState();
}

class _SwitcherState extends State<Switcher> {
  bool state = false;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          RegularText('${widget.title}'),
          CupertinoSwitch(
              value: state,
              onChanged: (boole) {
                state = !state;
                setState(() {});
              }),
        ],
      ),
    );
  }
}
