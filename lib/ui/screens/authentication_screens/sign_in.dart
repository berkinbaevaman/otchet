import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:otchet/ui/screens/authentication_screens/sign_up.dart';
import 'package:otchet/ui/screens/main_pages.dart';
import 'package:otchet/ui/theme/input_formatters.dart';
import 'package:otchet/ui/widgets/buttons/action_button.dart';
import 'package:otchet/ui/widgets/input_field_widget.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class SignInPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ProtoScaffold(
        appBarTitle: '',
        body: SingleChildScrollView(
          child: Column(
            children: [
              Row(
                children: [
                  SizedBox(
                    width: 26,
                  ),
                  Container(
                      width: MediaQuery.of(context).size.width * 0.4,
                      child: Image.asset('assets/logo.png')),
                ],
              ),
              Row(
                children: [
                  SizedBox(
                    height: 60,
                    width: 26,
                  ),
                  HeaderText('Авторизация'),
                ],
              ),
              CustomTextField(
                  headText: 'ИИН',
                  hintText: 'XXXX-XXXX-XXXX',
                  maxChar: 14,
                  inputType: TextInputType.number,
                  inputFormatter: [
                    // FilteringTextInputFormatter.digitsOnly,
                    iinFormatter,
                  ],
                  onChanged: (e) {
                    // iin = e;
                  }),
              CustomTextField(
                hintText: 'Пароль',
                obscure: true,
                inputFormatter: [
                  // FilteringTextInputFormatter
                ],
                onChanged: (e) {
                  print('  ');
                },
                headText: 'Пароль',
              ),
              Row(
                children: [
                  // ColoredButtonText(text: 'Забыли пароль?', color: Colors.blue),
                  SizedBox(
                    width: 40,
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.end,
              ),
              SizedBox(
                height: 10,
              ),
              ActionButton(
                  label: 'Войти',
                  onPressed: () {
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) => MainPage()));
                  }),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  SizedBox(
                    width: 26,
                  ),
                  Text('У вас нет аккаунта? '),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SignUpPage()));
                    },
                    child: ColoredButtonText(
                        text: 'Зарегистрироваться', color: Colors.blue),
                  )
                ],
              ),
            ],
          ),
        ));
  }
}
