import 'package:flutter/material.dart';
import 'package:otchet/ui/screens/authentication_screens/create_password.dart';
import 'package:otchet/ui/widgets/buttons/action_button.dart';
import 'package:otchet/ui/widgets/ecp_field.dart';
import 'package:otchet/ui/widgets/input_field_widget.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class SignUpPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ProtoScaffold(
        appBarTitle: '',
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  SizedBox(
                    width: 26,
                  ),
                  Container(
                      width: MediaQuery.of(context).size.width * 0.4,
                      child: Image.asset('assets/logo.png')),
                ],
              ),
              Row(
                children: [
                  SizedBox(
                    height: 60,
                    width: 26,
                  ),
                  HeaderText('Регистрация'),
                ],
              ),
              ECPField('ЭЦП'),
              CustomTextField(
                hintText: 'mail@gmail.com',
                headText: 'E-mail',
                onChanged: (e) {
                  print('  ');
                },
              ),
              CustomTextField(
                hintText: 'Код',
                headText: 'Код органа гос.доходов',
                onChanged: (e) {
                  print('  ');
                },
              ),
              CustomTextField(
                hintText: 'Код',
                headText: 'Код органа гос.доходов по месту жительства',
                onChanged: (e) {
                  print('  ');
                },
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Checkbox(value: true, onChanged: (_) {}),
                  Text('Resident'),
                  Checkbox(value: false, onChanged: (_) {}),
                  Text('Not Resident'),
                ],
              ),
              ActionButton(
                  label: 'Далее',
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CreatePasswordPage()));
                  }),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16.0),
                child: Row(
                  children: [
                    Text('У Вас уже есть аккаунт? '),
                    InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: ColoredButtonText(
                            text: 'Войти', color: Colors.blue)),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16.0),
                child: Text('Хотите узнать о приложении подробнее? '),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16.0),
                child:
                    ColoredButtonText(text: 'Демо версия', color: Colors.blue),
              ),
            ],
          ),
        ));
  }
}

// Padding(
//   padding: const EdgeInsets.all(8.0),
//   child: Text('Забыли пароль?'),
// ),
// Padding(
//   padding: const EdgeInsets.all(8.0),
//   child: Text('Войти с помощью соцсетей'),
// ),
// Row(
//   mainAxisAlignment: MainAxisAlignment.center,
//   children: [
//     Container(
//         width: MediaQuery.of(context).size.width / 2.5,
//         child: SocialLoginButton(
//             label: 'Facebook',
//             onPressed: () {},
//             icon: 'assets/facebook.png')),
//     SizedBox(
//       width: 16,
//     ),
//     Container(
//       width: MediaQuery.of(context).size.width / 2.5,
//       child: SocialLoginButton(
//           label: 'Google',
//           onPressed: () {},
//           icon: 'assets/google.png'),
//     ),
//   ],
// ),
// Padding(
//   padding: const EdgeInsets.symmetric(horizontal: 34.0),
//   child: Divider(
//     color: Colors.black,
//   ),
// ),
// Padding(
//   padding: const EdgeInsets.symmetric(horizontal: 26.0),
//   child: Row(
//     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//     children: [
//       Padding(
//         padding: const EdgeInsets.all(8.0),
//         child: Text('Нет аккаунта?'),
//       ),
//       Padding(
//         padding: const EdgeInsets.all(8.0),
//         child: Text('Регистрация'),
//       ),
//     ],
//   ),
// ),
