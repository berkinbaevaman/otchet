import 'package:flutter/material.dart';
import 'package:otchet/ui/screens/documents_screens/creating_tab/document_creating_page.dart';
import 'package:otchet/ui/screens/documents_screens/widgets/doc_tile.dart';

class CreateDocOptionList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => DocumentCreatingPage()));
      },
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          ListView.builder(
              shrinkWrap: true,
              itemCount: 6,
              physics: NeverScrollableScrollPhysics(),
              // padding: EdgeInsets.symmetric(vertical: 3, horizontal: 16),
              itemBuilder: (context, index) {
                return DocumentTile(names[index]);
              })
        ],
      ),
    );
  }
}

const List<String> names = [
  'Акты выполненных работ',
  'Счета на оплату',
  'Счет-фактура',
  'Накладные',
  'Приходные кассовые ордеры',
  'Акты сверки',
  'Доверенность'
];
