import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:otchet/ui/screens/documents_screens/creating_tab/popPages/goods_services.dart';
import 'package:otchet/ui/screens/home/warehouse_screens/warehouse_main_page.dart';
import 'package:otchet/ui/widgets/buttons/action_button.dart';
import 'package:otchet/ui/widgets/ecp_field.dart';
import 'package:otchet/ui/widgets/input_field_widget.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';
import 'package:otchet/date_picker.dart' as cupertinoPicker;

class DocumentCreatingPage extends StatelessWidget {
  final TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return ProtoScaffold(
        appBarTitle: 'Акты выполненных работ',
        body: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 8),
              CustomTextField(
                headText: 'Наименование организации заказчика*',
                hintText: 'Lorem',
                onChanged: (text) {},
              ),
              CustomTextField(
                headText: 'Бин*',
                hintText: 'ХХХХХХХХХХХХ',
                onChanged: (text) {},
              ),
              CustomTextField(
                headText: 'Адрес*',
                hintText: 'Введите адрес',
                onChanged: (text) {},
              ),
              CustomTextField(
                headText: 'Договор (контракт)',
                hintText: 'Lorem',
                onChanged: (text) {},
              ),
              CustomTextField(
                headText: 'Номер документа*',
                hintText: '№000',
                onChanged: (text) {},
              ),
              InkWell(
                onTap: () {
                  showDatePicker(context);
                },
                child: DumbField(
                  headText: 'Дата составления*',
                  dataText: 'ДД.ММ.ГГ',
                  // onChanged: (text) {},
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      CustomMaterialPageRoute(
                          builder: (context) => ServicesAndGoodsOptionPage()));
                },
                child: DumbField(
                  headText: 'Товары/услуги*',
                  dataText: 'Товары/услуги*',
                  // onChanged: (text) {},
                ),
              ),
              ECPField('Подписать с ЭЦП*'),
              ColoredButtonText(text: 'Предпросмотр', color: Colors.blue),
              ActionButton(label: 'Отправить', onPressed: () {}),
              SizedBox(
                height: 50,
              )
            ],
          ),
        ));
  }
}

showDatePicker(context) {
  showCupertinoDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        DateTime time = DateTime.now();
        bool change = true;
        double width = MediaQuery.of(context).size.width - 32;
        return StatefulBuilder(builder: (context, setState) {
          return AlertDialog(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () {
                    setState(() {
                      change = !change;
                    });
                  },
                  child: Text(
                    '${time.month.toString()} ${time.year}',
                    style: TextStyle(color: Colors.blue),
                  ),
                ),
              ],
            ),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  height: 200,
                  width: width,
                  child: change
                      ? CalendarCarousel<Event>(
                          onDayPressed: (DateTime date, List<Event> events) {
                            setState(() {
                              time = date;
                            });
                          },
                          todayBorderColor: Colors.transparent,
                          showHeaderButton: false,
                          showHeader: false,
                          weekdayTextStyle: TextStyle(color: Colors.black),
                          headerMargin: EdgeInsets.all(8),
                          weekendTextStyle: TextStyle(
                            color: Colors.black,
                          ),
                          markedDateIconBorderColor: Colors.transparent,
                          firstDayOfWeek: 1,
                          todayButtonColor: Colors.blue,
                          showOnlyCurrentMonthDate: true,
                          todayTextStyle: TextStyle(color: Colors.white),
                          thisMonthDayBorderColor: Colors.transparent,
                          weekFormat: false,
                          height: 300.0,
                          daysHaveCircularBorder: true,
                        )
                      : cupertinoPicker.CupertinoDatePicker(
                          minimumDate: DateTime.utc(1950),
                          maximumDate: DateTime.utc(2030),
                          initialDateTime:
                              time.year < 1951 ? DateTime.utc(1950) : time,
                          minimumYear: 1950,
                          maximumYear: 2030,
                          onDateTimeChanged: (DateTime timeIs) {
                            setState(() {
                              time = timeIs;
                            });
                          },
                          mode: cupertinoPicker.CupertinoDatePickerMode.date,
                        ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ColoredButtonText(
                      text: 'Сбросить',
                      color: Colors.blue,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: ColoredButtonText(
                        text: 'ОК',
                        color: Colors.blue,
                      ),
                    )
                  ],
                )
              ],
            ),
          );
        });
      });
}
