import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:otchet/ui/widgets/buttons/action_button.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';
import 'package:otchet/ui/widgets/searchbar.dart';

class ServicesAndGoodsOptionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ProtoScaffold(
      appBarTitle: 'Товары/услуги',
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ActionButton(label: 'Добавить товар/услугу', onPressed: () {}),
            SearchBar(),
            Flexible(
              flex: 1,
              child: ListView.separated(
                // padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
                itemCount: 6,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  if (index == 5)
                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: ListTile(
                        title: Text('item title $index'),
                        trailing: Wrap(
                          children: [
                            Icon(
                              CupertinoIcons.minus_circle_fill,
                              color: Colors.blue,
                            ),
                            SizedBox(width: 4),
                            Text('1'),
                            SizedBox(width: 4),
                            Icon(
                              CupertinoIcons.add_circled_solid,
                              color: Colors.blue,
                            )
                          ],
                        ),
                      ),
                    );
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: ListTile(
                      title: Text('item title$index'),
                      trailing: Icon(
                        CupertinoIcons.add,
                        color: Colors.blue,
                      ),
                    ),
                  );
                },
                separatorBuilder: (context, index) {
                  return Divider();
                },
              ),
            )
          ],
        ),
      ),
      navBar: Padding(
        padding: const EdgeInsets.only(bottom: 30.0),
        child: ActionButton(label: 'Выбрать', onPressed: () {}),
      ),
    );
  }
}

// class _Items extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Container();
//   }
// }
