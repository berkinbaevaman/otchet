import 'package:flutter/material.dart';
import 'package:otchet/ui/widgets/scaffold_widgets/scaffold_proto.dart';
import 'package:otchet/ui/widgets/searchbar.dart';
import 'package:otchet/ui/widgets/shadowed_container.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class DocListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ProtoScaffold(
        appBarTitle: 'Акты выполненных работ',
        body: SingleChildScrollView(
          child: Column(
            children: [
              SearchBar(),
              ListView.builder(
                  itemCount: 4,
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return _DocListTile();
                  })
            ],
          ),
        ));
  }
}

class _DocListTile extends StatelessWidget {
  Widget binInfoField() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        RichText(
            text: TextSpan(
                text: 'БИН: ',
                style: TextStyle(color: Colors.grey),
                children: [
              TextSpan(
                  text: '1007-4000-1310',
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w700,
                      color: Colors.black)),
            ])),
        Text('N23')
      ],
    );
  }

  Widget companyNameField() {
    return RegularText('ТОО "ALLMED FZCO.KZ"');
  }

  Widget dateAndPriceField() {
    return Row(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            LightSubtitleText('Дата составления:'),
            SizedBox(
              height: 5,
            ),
            RegularText('12.12.2012')
          ],
        ),
        SizedBox(width: 46),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            LightSubtitleText('Стоимость:'),
            SizedBox(
              height: 5,
            ),
            RegularText('10000T')
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return ShadowedContainer(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        binInfoField(),
        SizedBox(
          height: 8,
        ),
        companyNameField(),
        SizedBox(
          height: 13,
        ),
        dateAndPriceField()
      ],
    ));
  }
}
