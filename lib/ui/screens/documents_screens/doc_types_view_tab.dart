import 'package:flutter/material.dart';
import 'package:otchet/ui/screens/documents_screens/doc_creating_options_tab.dart';
import 'package:otchet/ui/screens/documents_screens/view_tab/view_exact_type_docs.dart';
import 'package:otchet/ui/screens/home/warehouse_screens/warehouse_main_page.dart';
import 'package:otchet/ui/widgets/shadowed_container.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class ShowDocuments extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: names.length,
        padding: EdgeInsets.fromLTRB(0, 16, 0, 16),
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (context, index) {
          return _DocTypeTile(
            names[index],
            index: index + 1,
          );
        });
  }
}

class _DocTypeTile extends StatelessWidget {
  final String name;
  final int index;

  const _DocTypeTile(this.name, {Key? key, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(context, CustomMaterialPageRoute(builder: (context) {
          return DocListView();
        }));
      },
      child: ShadowedContainer(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              RegularText(name),
              Icon(
                Icons.arrow_forward_ios,
                size: 14,
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          RichText(
              text: TextSpan(
                  text: 'Кол-во: ',
                  style: TextStyle(color: Colors.grey),
                  children: [
                TextSpan(
                    text: '${(21 * 3 * index) % 100}',
                    style: TextStyle(
                        color: Colors.blue,
                        fontSize: 15,
                        fontWeight: FontWeight.w700)),
              ]))
        ],
      )),
    );
  }
}
