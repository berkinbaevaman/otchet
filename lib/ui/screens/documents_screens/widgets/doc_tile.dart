import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:otchet/ui/theme/styles.dart';
import 'package:otchet/ui/widgets/shadowed_container.dart';

class DocumentTile extends StatelessWidget {
  final String name;
  const DocumentTile(this.name);
  @override
  Widget build(BuildContext context) {
    return ShadowedContainer(
        padding: EdgeInsets.all(14),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(name),
            Container(
                decoration: BoxDecoration(
                  color: Colors.blue,
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                child: Icon(
                  CupertinoIcons.add,
                  color: Colors.white,
                )),
          ],
        ));
  }
}
