import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:otchet/ui/screens/documents_screens/doc_creating_options_tab.dart';
import 'package:otchet/ui/screens/documents_screens/doc_types_view_tab.dart';
import 'package:otchet/ui/widgets/text_widgets.dart';

class DocsMainPage extends StatelessWidget {
  const DocsMainPage();

  BoxDecoration _indicator() {
    return BoxDecoration(
        borderRadius: BorderRadius.circular(8.9),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.2),
            spreadRadius: 1,
            blurRadius: 1,
            offset: Offset(0, 0), // changes position of shadow
          ),
        ],
        color: Colors.white);
  }

  @override
  Widget build(BuildContext context) {
    /// edit [_DocsBody] to add additional items to content of tabbar
    return _DocsBody(
      tabBar: TabBar(
        unselectedLabelColor: Colors.black,
        indicatorSize: TabBarIndicatorSize.tab,
        indicator: _indicator(),
        onTap: (index) async {},
        tabs: [Text('Создать документ'), Text('Документы')],
      ),
      content: TabBarView(
        children: [
          CreateDocOptionList(),
          ShowDocuments(),
        ],
      ),
    );
  }
}

class _DocsBody extends StatelessWidget {
  final Widget tabBar;
  final Widget content;
  _DocsBody({required this.tabBar, required this.content});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 22.0, left: 16, bottom: 16),
              child: MainTitleText('Документы'),
            ),
            DefaultTabController(
              length: 2,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 2.0, horizontal: 2.0),
                    margin: EdgeInsets.symmetric(horizontal: 16),
                    height: 40,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8.9)),
                        color: Color(0xffE9E9E9)),
                    child: tabBar,
                  ),
                  SizedBox(height: 1000, child: content)
                ],
              ),
            ),
            //List of questions
          ],
        )),
      ),
    );
  }
}
