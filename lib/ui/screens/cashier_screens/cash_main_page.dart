import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:otchet/ui/widgets/shadowed_container.dart';

class CashMainPage extends StatelessWidget {
  const CashMainPage();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: 16,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16),
            child: Text(
              'Касса',
              style: TextStyle(fontSize: 34, fontWeight: FontWeight.w700),
            ),
          ),
          SizedBox(
            height: 16,
          ),
          ListView.builder(
              shrinkWrap: true,
              itemCount: 6,
              itemBuilder: (context, index) {
                return ShadowedContainer(
                    child: Row(
                  children: [
                    Icon(
                      Icons.assignment_outlined,
                      color: Colors.blue,
                    ),
                    SizedBox(width: 12),
                    Text(_cashNames[index]),
                    Expanded(flex: 1, child: Container()),
                    Icon(Icons.arrow_forward_ios)
                  ],
                ));
              })
          //List of questions
        ],
      )),
    );
  }
}

List<String> _cashNames = [
  'Z отчет',
  'X отчет',
  'Фискальный отчет',
  'Отчет по кассирам',
  'Отчет по товарам',
  'Фискальный чек'
];
enum ReportType {
  Report_Z,
  Report_X,
  Fiscal_Report,
  Cashier_Report,
  Goods_Report,
  Fiscal_Check
}
