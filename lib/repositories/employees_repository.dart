import 'dart:async';
import 'package:otchet/models/employee_model.dart';

// typedef Future<List<Employee>> Func(List<Employee> list, Employee newEmployee);

class EmployeesRepository {
  const EmployeesRepository();

  Future<List<Employee>> getEmployeesFromRemoteData() async {
    List<Employee> fetchedEmployees = [];
    await Future.delayed(Duration(milliseconds: 700));

    Employee _employeesData = Employee(
        'Амантур Беркинбаев Нарбайулы', '1 500 000 тг', '9603-0230-0665');

    for (int i = 0; i < 2; i++) {
      fetchedEmployees
          .add(_employeesData.copyWith(salary: (500000 * (i + 1)).toString()));
    }
    return fetchedEmployees;
  }

  //edit state(add data) and send to server
  Future<List<Employee>> addEmployee(
      List<Employee> list, Employee newEmployee) async {
    await Future.delayed(Duration(milliseconds: 700));
    var newList = list;
    newList.add(newEmployee);
    return newList;
  }

  //edit existing data and send to server
  Future<List<Employee>> editEmployee(
      List<Employee> list, Employee prevObject, curObject) async {
    int index = list.indexOf(prevObject);
    list[index] = curObject;
    return list;
  }

  //delete data from state and send to server
  Future<List<Employee>> deleteEmployee(
      List<Employee> list, Employee targetEmployee) async {
    await Future.delayed(Duration(milliseconds: 700));
    list.remove(targetEmployee);

    return list;
  }
}
