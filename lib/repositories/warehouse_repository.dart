import 'dart:async';
import 'dart:math';

import 'package:otchet/models/ware_model.dart';

class WarehouseRepository {
  const WarehouseRepository();

  Future<List<Ware>> getWares() async {
    List<Ware> list = [];

    await Future.delayed(Duration(milliseconds: 700));
    Map<String, dynamic> wareItem = {
      'modelName': 'Scarlett SC-EK21S26 серебристый',
      'barcode': '12345678',
      'price': 10000,
      'quantity': 12,
      'brand': 'Scarlett',
      'color': 'Glue'
    };

    Ware _wareData = Ware.fromJson(wareItem);

    for (int i = 0; i < 6; i++) {
      list.add(_wareData.copyWith(
          price: Random().nextInt(20000),
          barcode: (Random().nextInt(999999)).toString(),
          quantity: Random().nextInt(200)));
    }
    print(list);
    return list;
  }

  Future<List<Ware>> addWare(List<Ware> list, Ware newEmployee) async {
    await Future.delayed(Duration(milliseconds: 700));
    var newList = list;
    newList.add(newEmployee);
    return newList;
  }

  //edit existing data and send to server
  Future<List<Ware>> editWare(
      List<Ware> list, Ware prevObject, curObject) async {
    int index = list.indexOf(prevObject);
    list[index] = curObject;
    return list;
  }

  //delete data from state and send to server
  Future<List<Ware>> deleteWare(List<Ware> list, Ware targetEmployee) async {
    await Future.delayed(Duration(milliseconds: 700));
    list.remove(targetEmployee);

    return list;
  }
}
