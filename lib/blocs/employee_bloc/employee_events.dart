import 'package:equatable/equatable.dart';
import 'package:otchet/models/employee_model.dart';

abstract class EmployeeEvent extends Equatable {
  const EmployeeEvent();
  @override
  List<Object> get props => [];
}

//create
class AddNewEmployeeEvent extends EmployeeEvent {
  final Employee _newEmployee;

  AddNewEmployeeEvent(this._newEmployee);

  Employee get newEmployee => _newEmployee;
  List<Object> get props => [_newEmployee];
}

//read
class LoadEmployeesEvent extends EmployeeEvent {
  const LoadEmployeesEvent();
}

//update
class EditEmployeeEvent extends EmployeeEvent {
  // final List<Employee> employeeList;
  final Employee _targetEmployee;
  final Employee _newEmployee;

  const EditEmployeeEvent(this._targetEmployee, this._newEmployee);

  Employee get targetEmployee => _targetEmployee;
  Employee get newEmployee => _newEmployee;
  List<Object> get props => [_targetEmployee, _newEmployee];
}

//delete
class DeleteEmployeeEvent extends EmployeeEvent {
  final Employee _targetEmployee;

  const DeleteEmployeeEvent(this._targetEmployee);

  Employee get targetEmployee => _targetEmployee;

  @override
  List<Object> get props => [_targetEmployee];
}
