import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otchet/models/employee_model.dart';
import 'package:otchet/repositories/employees_repository.dart';
import 'employee_events.dart';
import 'employee_state.dart';

class EmployeeBloc extends Bloc<EmployeeEvent, EmployeeState> {
  final EmployeesRepository _employeesRepository;

  EmployeeBloc({
    required EmployeesRepository employeesRepository,
  })   : _employeesRepository = employeesRepository,
        super(EmployeeInitialEmptyState());

  @override
  Stream<EmployeeState> mapEventToState(EmployeeEvent event) async* {
    var currentState = state;
    if (currentState is EmployeesDataState) {
      try {
        //Если вы добавите новые ивенты нужно
        //не забудьте добавить хэндлер для этого ивента в [_mapEventToChangeData(currentState, event)]
        //в ином случае будет бросаться начальный стэйт
        yield await _mapEventToChangeData(currentState, event);
      } on Exception catch (e) {
        print('catch exception');
        print(e);
        yield EmployeeInitialEmptyState();
        add(LoadEmployeesEvent());
      }
    } else if (currentState is EmployeeInitialEmptyState) {
      yield await _getEmployeesToState();
    } else {
      print('$event was not implemented');
      yield const EmployeeErrorState();
    }
  }

  Future<EmployeeState> _mapEventToChangeData(
      EmployeesDataState curState, event) async {
    if (event is EditEmployeeEvent) {
      return await _editEmployee(event, curState);
    } else if (event is AddNewEmployeeEvent) {
      return _addEmployee(event, curState);
    } else if (event is DeleteEmployeeEvent) {
      return await _deleteEmployee(event, curState);
    } else if (event is LoadEmployeesEvent) {
      return await _getEmployeesToState();
    }
    throw Exception(
        '${event.runtimeType} has no implementation for ${curState.runtimeType}');
    // throw Exception('Could not find $event for ${curState.runtimeType}');
  }

  //fetch Data from server:
  Future<EmployeeState> _getEmployeesToState() async {
    try {
      var _data = await _employeesRepository.getEmployeesFromRemoteData();
      return EmployeesDataState(_data);
    } catch (_) {
      print(_.toString());
      return const EmployeeErrorState();
    }
  }

  //create new employee and send to data server:
  Future<EmployeeState> _addEmployee(
      AddNewEmployeeEvent event, EmployeesDataState currentState) async {
    try {
      List<Employee> curStateList = List.from(currentState.employees);
      List<Employee> newEmployees = await _employeesRepository.addEmployee(
          curStateList, event.newEmployee);

      return EmployeesDataState(newEmployees);
    } catch (_) {
      print(_.toString());
      return const EmployeeErrorState();
    }
  }

  //update existing employee and sync with data server:
  Future<EmployeeState> _editEmployee(
      EditEmployeeEvent event, EmployeesDataState currentState) async {
    try {
      List<Employee> newList = List.from(currentState.employees);
      var tempEmployees = await _employeesRepository.editEmployee(
          newList, event.targetEmployee, event.newEmployee);
      return new EmployeesDataState(tempEmployees);
    } catch (_) {
      print(_.toString());
      return const EmployeeErrorState();
    }
  }

  //delete employee and sync with server:
  Future<EmployeeState> _deleteEmployee(
      DeleteEmployeeEvent event, EmployeesDataState currentState) async {
    try {
      final List<Employee> tempEmployees =
          await _employeesRepository.deleteEmployee(
              List.from(currentState.employees), event.targetEmployee);
      return EmployeesDataState(tempEmployees);
    } catch (_) {
      print(_.toString());
      return const EmployeeErrorState();
    }
  }
}

// class EventException with Exception{
//   EmployeeEvent _event;
//
//   EmployeeEvent get event => _event;
//
//   set event(EmployeeEvent event) {
//     _event = event;
//   }
//
//   @override
//   String toString() {
//
//     return '${event.runtimeType} has no implementation';
//   }
//
//
// }
