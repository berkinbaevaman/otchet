import 'package:equatable/equatable.dart';
import 'package:otchet/models/employee_model.dart';

abstract class EmployeeState extends Equatable {
  const EmployeeState();
  @override
  List<Object?> get props => [];
}

class EmployeeInitialEmptyState extends EmployeeState {
  const EmployeeInitialEmptyState();
}

class EmployeeLoadingState extends EmployeeState {
  const EmployeeLoadingState();
}

class EmployeesDataState extends EmployeeState {
  final List<Employee> _employees;

  List<Employee> get employees {
    List<Employee> newEmployee = _employees;
    return newEmployee;
  }

  const EmployeesDataState(this._employees);

  @override
  List<Object?> get props => [_employees];
}

class EmployeeErrorState extends EmployeeState {
  const EmployeeErrorState();
}
