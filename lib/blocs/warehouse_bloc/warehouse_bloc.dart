import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otchet/models/ware_model.dart';
import 'package:otchet/repositories/warehouse_repository.dart';
import 'warehouse_events.dart';
import 'warehous_state.dart';

class WarehouseBloc extends Bloc<WarehouseEvent, WarehouseState> {
  final WarehouseRepository _warehouseRepository;

  WarehouseBloc({
    required WarehouseRepository warehouseRepository,
  })   : _warehouseRepository = warehouseRepository,
        super(WarehouseInitialEmptyState());

  @override
  Stream<WarehouseState> mapEventToState(WarehouseEvent event) async* {
    var currentState = state;
    print("${state.runtimeType} ${event.runtimeType}");
    if (currentState is WarehouseLoadedState) {
      try {
        //Если вы добавите новые ивенты нужно
        //не забудьте добавить хэндлер для этого ивента в [_mapEventToChangeData(currentState, event)]
        //в ином случае будет бросаться начальный стэйт
        yield await _mapEventToChangeData(currentState, event);
      } on Exception catch (e) {
        print('catch exception');
        print(e);
        yield WarehouseInitialEmptyState();
        add(LoadWarehouseEvent());
      }
    } else if (currentState is WarehouseInitialEmptyState) {
      yield await _getEmployeesToState();
    } else {
      print('$event was not implemented');
      yield const WarehouseErrorState();
    }
  }

  Future<WarehouseState> _mapEventToChangeData(
      WarehouseLoadedState curState, event) async {
    if (event is EditWarehouseEvent) {
      return await _editEmployee(event, curState);
    } else if (event is AddWarehouseItemEvent) {
      return _addEmployee(event, curState);
    } else if (event is DeleteFromWarehouseEvent) {
      return await _deleteEmployee(event, curState);
    } else if (event is LoadWarehouseEvent) {
      return await _getEmployeesToState();
    }
    throw Exception(
        '${event.runtimeType} has no implementation for ${curState.runtimeType}');
  }

  //fetch Data from server:
  Future<WarehouseState> _getEmployeesToState() async {
    try {
      List<Ware> _data = await _warehouseRepository.getWares();
      return WarehouseLoadedState(_data);
    } catch (_) {
      print(_.toString());
      return const WarehouseErrorState();
    }
  }

  //create new employee and send to data server:
  Future<WarehouseState> _addEmployee(
      AddWarehouseItemEvent event, WarehouseLoadedState currentState) async {
    try {
      List<Ware> curStateList = List.from(currentState.wareList);
      List<Ware> newEmployees =
          await _warehouseRepository.addWare(curStateList, event.newWare);

      return WarehouseLoadedState(newEmployees);
    } catch (_) {
      print(_.toString());
      return const WarehouseErrorState();
    }
  }

  //update existing employee and sync with data server:
  Future<WarehouseState> _editEmployee(
      EditWarehouseEvent event, WarehouseLoadedState currentState) async {
    try {
      List<Ware> newList = List.from(currentState.wareList);
      var tempEmployees = await _warehouseRepository.editWare(
          newList, event.targetWare, event.newWare);
      return new WarehouseLoadedState(tempEmployees);
    } catch (_) {
      print(_.toString());
      return const WarehouseErrorState();
    }
  }

  //delete employee and sync with server:
  Future<WarehouseState> _deleteEmployee(
      DeleteFromWarehouseEvent event, WarehouseLoadedState currentState) async {
    try {
      final List<Ware> tempEmployees = await _warehouseRepository.deleteWare(
          List.from(currentState.wareList), event.targetWare);
      return WarehouseLoadedState(tempEmployees);
    } catch (_) {
      print(_.toString());
      return const WarehouseErrorState();
    }
  }
}

// class EventException with Exception{
//   EmployeeEvent _event;
//
//   EmployeeEvent get event => _event;
//
//   set event(EmployeeEvent event) {
//     _event = event;
//   }
//
//   @override
//   String toString() {
//
//     return '${event.runtimeType} has no implementation';
//   }
//
//
// }
