import 'package:equatable/equatable.dart';
import 'package:otchet/models/ware_model.dart';

abstract class WarehouseEvent extends Equatable {
  const WarehouseEvent();
  @override
  List<Object> get props => [];
}

//create
class AddWarehouseItemEvent extends WarehouseEvent {
  final Ware _newWare;

  AddWarehouseItemEvent(this._newWare);

  Ware get newWare => _newWare;
  List<Object> get props => [_newWare];
}

//read
class LoadWarehouseEvent extends WarehouseEvent {
  const LoadWarehouseEvent();
}

//update
class EditWarehouseEvent extends WarehouseEvent {
  // final List<Employee> employeeList;
  final Ware _targetWare;
  final Ware _newWare;

  const EditWarehouseEvent(this._targetWare, this._newWare);

  Ware get targetWare => _targetWare;
  Ware get newWare => _newWare;
  List<Object> get props => [_targetWare, _newWare];
}

//delete
class DeleteFromWarehouseEvent extends WarehouseEvent {
  final Ware _targetWare;

  const DeleteFromWarehouseEvent(this._targetWare);

  Ware get targetWare => _targetWare;

  @override
  List<Object> get props => [_targetWare];
}
