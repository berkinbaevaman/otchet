import 'package:equatable/equatable.dart';
import 'package:otchet/models/ware_model.dart';

abstract class WarehouseState extends Equatable {
  const WarehouseState();
  @override
  List<Object?> get props => [];
}

class WarehouseInitialEmptyState extends WarehouseState {
  const WarehouseInitialEmptyState();
}

class WarehouseLoadingState extends WarehouseState {
  const WarehouseLoadingState();
}

class WarehouseLoadedState extends WarehouseState {
  final List<Ware> _wareList;

  List<Ware> get wareList => _wareList;

  const WarehouseLoadedState(this._wareList);

  @override
  List<Object?> get props => [_wareList];
}

class WarehouseErrorState extends WarehouseState {
  const WarehouseErrorState();
}
