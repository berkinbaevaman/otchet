import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:otchet/blocs/employee_bloc/employee_bloc.dart';
import 'package:otchet/blocs/warehouse_bloc/warehouse_bloc.dart';
import 'package:otchet/repositories/employees_repository.dart';
import 'package:otchet/repositories/warehouse_repository.dart';
import 'package:otchet/services/fcm.dart';
import 'package:otchet/ui/screens/authentication_screens/sign_in.dart';
import 'package:otchet/ui/screens/cashier_screens/cash_main_page.dart';
import 'package:otchet/ui/screens/home/employee_screens/employee_main_page.dart';
import 'package:otchet/ui/screens/profile_screens/creditnails_page.dart';
import 'package:otchet/ui/screens/profile_screens/localization_page.dart';
import 'package:otchet/ui/screens/profile_screens/notifications_page.dart';
import 'package:otchet/ui/screens/profile_screens/shops_page.dart';

late FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  initialize();
  runApp(const _MyApp(EmployeesRepository(), WarehouseRepository()));
}

class _MyApp extends StatelessWidget {
  final EmployeesRepository _employeesRepository;
  final WarehouseRepository _warehouseRepository;

  const _MyApp(this._employeesRepository, this._warehouseRepository);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => WarehouseBloc(warehouseRepository: _warehouseRepository),
      child: BlocProvider(
        lazy: true,
        create: (_) => EmployeeBloc(employeesRepository: _employeesRepository),
        child: MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'От4ет',
            theme: ThemeData(
              textTheme: TextTheme(),
              fontFamily: 'OpenSans',
              appBarTheme: AppBarTheme(
                iconTheme: IconThemeData(color: Colors.blue),
              ),
              primaryColor: Colors.white,
              buttonColor: Colors.blue,
              visualDensity: VisualDensity.adaptivePlatformDensity,
            ),
            routes: {
              EmployeesMainPage.title: (context) => EmployeesMainPage(),
              LocalizationPage.title: (context) => LocalizationPage(),
              CredentialsPage.title: (context) => CredentialsPage(),
              ShopList.title: (context) => ShopList(),
              NotificationPage.title: (context) => NotificationPage(),
              //need to implement routes later
              // EmployeeInfoPage.title: (context) => EmployeeInfoPage(), arguments required need to implement on generate route
            },
            home: SignInPage()),
      ),
    );
  }
}
